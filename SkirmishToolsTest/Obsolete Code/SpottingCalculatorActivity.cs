﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	[Activity (Label = "Spotting Calculator", Icon = "@drawable/ic_action_soldier")]
	public class SpottingCalculatorActivity : Activity
	{
		//SpottingCalculator spottingCalculator;
		CalculatorLogic spottingCalculator;

		public List<int> checkedBoxes = new List<int> ();

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.SpottingCalculator);

			spottingCalculator = new CalculatorLogic (this, CalculatorType.Spotting);

			// get all checkboxes and give them the OnCheckedChange

			ListView checkboxList = FindViewById<ListView> (Resource.Id.spottingList);


			CalculatorAdaptor adapter = new CalculatorAdaptor (this, CalculatorType.Spotting);

			checkboxList.Adapter = adapter;
			checkboxList.ChoiceMode = ChoiceMode.Multiple;

			Button pushButton = FindViewById<Button> (Resource.Id.spotCalculate);
			pushButton.Click += delegate {
				Calculate();
			};

		}

		public void Calculate ()
		{
			ListView checkboxList = FindViewById<ListView> (Resource.Id.spottingList);

			CalculatorAdaptor adapter = (CalculatorAdaptor)checkboxList.Adapter;

			for (int i = 0; i < adapter.items.Count; i++) {
				if (checkboxList.IsItemChecked (i)) {
					checkedBoxes.Add (i);
				}
			}

			int calculatedValue = spottingCalculator.Calculate (checkedBoxes, 100, 0);

			TextView result = FindViewById<TextView> (Resource.Id.spottingValuesField);

			result.Text = calculatedValue.ToString() + Resources.GetString (Resource.String.end_spotting);

			checkedBoxes.Clear ();
		}

		public void SetChecked(object sender, AdapterView.ItemClickEventArgs e)
		{
			ListView list = (ListView)sender;

			if (list.IsItemChecked(e.Position))
			{
				list.SetItemChecked(e.Position, false);

			}
			else
			{
				list.SetItemChecked(e.Position, true);
			}
		}
	}
		
}

