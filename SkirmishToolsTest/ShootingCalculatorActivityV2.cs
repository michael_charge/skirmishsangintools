
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	[Activity (Label = "Shooting Calculator", Icon = "@drawable/ic_action_soldier")]
	public class ShootingCalculatorV2Activity : Activity
	{
		//SpottingCalculator spottingCalculator;
		CalculatorLogicV2 calculatorLogic;

		Spinner aiming, targetStance, targetCover, shooterHealth, shooterStance, elevationDifference;
		MultiSelectSpinner shooterMisc, targetVehicle;
		CheckBox targetCloseBox, shooterVehicleMovingBox;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.ShootingCalculatorV2);

			calculatorLogic = new CalculatorLogicV2 (this, CalculatorType.Shooting);

			// Aiming
			aiming = FindViewById<Spinner> (Resource.Id.AimingSpinner);

			aiming.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			ArrayAdapter adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.shoot_aiming_array, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			aiming.Adapter = adapter;


			//Target Stance
			targetStance = FindViewById<Spinner> (Resource.Id.TargetStanceSpinner);

			targetStance.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Stances, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			targetStance.Adapter = adapter;

			//Target Cover
			targetCover = FindViewById<Spinner> (Resource.Id.TargetCoverSpinner);

			targetCover.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Cover, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			targetCover.Adapter = adapter;

			targetCloseBox = FindViewById<CheckBox> (Resource.Id.targetCloseCheckbox);

			//Shooter Health
			shooterHealth = FindViewById<Spinner> (Resource.Id.ShooterHealthSpinner);

			shooterHealth.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.healthStates, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			shooterHealth.Adapter = adapter;

			//Shooter Stance
			shooterStance = FindViewById<Spinner> (Resource.Id.ShooterStanceSpinner);

			shooterStance.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Stances, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			shooterStance.Adapter = adapter;

			//Shooter Misc
			shooterMisc = FindViewById<MultiSelectSpinner> (Resource.Id.ShooterMiscSpinner);

			shooterMisc.SetItems (Resources.GetStringArray(Resource.Array.shoot_shooter_misc));

			//Height Difference
			elevationDifference = FindViewById<Spinner> (Resource.Id.HeightDifferenceSpinner);

			elevationDifference.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Elevation, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);

			elevationDifference.Adapter = adapter;
			elevationDifference.SetSelection (3);

			//targetVehicle
			targetVehicle = FindViewById<MultiSelectSpinner> (Resource.Id.TargetVehicleSpinner);

			targetVehicle.SetItems (Resources.GetStringArray(Resource.Array.targetShootingVehicle));

			//spottingVehicle
			shooterVehicleMovingBox = FindViewById<CheckBox> (Resource.Id.shootingVehicleMovingCheckbox);

			// Calculate button
			Button pushButton = FindViewById<Button> (Resource.Id.spotCalculate);
			pushButton.Click += delegate {
				Calculate();
			};

		}

		public void Calculate ()
		{
			int shootingSkill;

			bool validShootingSkill = int.TryParse((FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text), out shootingSkill);

			if (!validShootingSkill) {
				shootingSkill = 22;
				FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text = "22";
			} else {
				if (shootingSkill < 22) {
					shootingSkill = 22;
					FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text = "22";
				} else if (shootingSkill > 150) {
					shootingSkill = 150;
					FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text = "150";
				}
			}

			//aiming
			int aimingValue = aiming.SelectedItemPosition;
			Modifiers aimingMod = Modifiers.None;

			switch (aimingValue) {
			case 0:
				aimingMod = Modifiers.None;
				break;
			case 1:
				aimingMod = Modifiers.ActiveSoldierAiming1;
				break;
			case 2:
				aimingMod = Modifiers.ActiveSoldierAiming2;
				break;
			}

			// target stance
			enum_Stances temp_targetStance = (enum_Stances)targetStance.SelectedItemPosition;
			Modifiers targetStanceMod = Modifiers.None;

			switch (temp_targetStance)
			{
				case enum_Stances.Standing:
					targetStanceMod = Modifiers.None;
					break;
				case enum_Stances.Running:
					targetStanceMod = Modifiers.TargetRunning;
					break;
				case enum_Stances.Walking:
					targetStanceMod = Modifiers.TargetWalking;
					break;
				case enum_Stances.Kneeling:
					targetStanceMod = Modifiers.TargetKneeling;
					break;
				case enum_Stances.Crawling:
					targetStanceMod = Modifiers.TargetCrawling;
					break;
				case enum_Stances.Prone:
					targetStanceMod = Modifiers.TargetProne;
					break;
				default:
					break;
			}

			// targetCover
			enum_Cover temp_targetCover = (enum_Cover) targetCover.SelectedItemPosition;
			Modifiers targetCoverMod = Modifiers.None;

			switch (temp_targetCover)
			{
				case enum_Cover.None:
					targetCoverMod = Modifiers.None;
					break;
				case enum_Cover.Soft:
					targetCoverMod = Modifiers.TargetInSoftCover;
					break;
				case enum_Cover.Medium:
					targetCoverMod = Modifiers.TargetInMediumCover;
					break;
				case enum_Cover.Hard:
					targetCoverMod = Modifiers.TargetInHardCover;
					break;
				case enum_Cover.Prepared:
					targetCoverMod = Modifiers.TargetInPreparedDefences;
					break;
				default:
					break;
			}

			// Target at close range
			bool targetClose = targetCloseBox.Checked;

			// spotterInjury
			InjuryLevel spotterInjuryMod = (InjuryLevel) shooterHealth.SelectedItemPosition;


			// shooterStance
			enum_Stances temp_shooterStance = (enum_Stances) shooterStance.SelectedItemPosition;
			Modifiers shooterStanceMod = Modifiers.None;

			switch (temp_shooterStance)
			{
				case enum_Stances.Standing:
				case enum_Stances.Kneeling:
				case enum_Stances.Prone:
					shooterStanceMod = Modifiers.None;
					break;
				case enum_Stances.Running:
					shooterStanceMod = Modifiers.ActiveSoldierRunning;
					break;
				case enum_Stances.Walking:
				case enum_Stances.Crawling:
					shooterStanceMod = Modifiers.ActiveSoldierWalking;
					break;
				default:
					break;
			}

			// spotterMisc Multi
			List<int> shooterMiscIndexs = shooterMisc.GetSelectedIndicies();
			List<Modifiers> shooterMiscMod = new List<Modifiers>();

			foreach (int i in shooterMiscIndexs)
			{
				enum_ShootingShooterMisc shootingMisc = (enum_ShootingShooterMisc) i;

				switch (shootingMisc)
				{
				case enum_ShootingShooterMisc.SSniperScope:
					shooterMiscMod.Add (Modifiers.ActiveSoldierUsingSniperScope);
					break;
				case enum_ShootingShooterMisc.SRidingAnimal:
					shooterMiscMod.Add (Modifiers.ActiveSoldierRidingAnimal);
					break;
				case enum_ShootingShooterMisc.SSnapFire:
					shooterMiscMod.Add (Modifiers.ActiveSoldierSnapFire);
					break;
				case enum_ShootingShooterMisc.SWeaponBraced:
					shooterMiscMod.Add (Modifiers.ActiveSoldierWeaponBraced);
					break;
				}

			}

			// elevationDifferenes
			enum_elevationDifferences temp_elevation = (enum_elevationDifferences) elevationDifference.SelectedItemPosition;

			// vehiclesTarget Multi
			List<int> targetMiscVIndex = targetVehicle.GetSelectedIndicies();
			List<Modifiers> vehiclesTargetMod = new List<Modifiers>();

			foreach (int i in targetMiscVIndex)
			{
				enum_ShootingTargetV targetMiscV = (enum_ShootingTargetV)i;
				{
					switch (targetMiscV)
					{
					case enum_ShootingTargetV.TVHardCover:
						vehiclesTargetMod.Add (Modifiers.TargetVehicleHardCover);
						break;
					case enum_ShootingTargetV.TVSoftCover:
						vehiclesTargetMod.Add (Modifiers.TargetVehicleSoftCover);
						break;
					case enum_ShootingTargetV.TVMoving:
						break;
					}
				}

			}
				

			bool shootingVehicleMoving = shooterVehicleMovingBox.Checked;

			int calculatedValue = calculatorLogic.CalculateShooting (shootingSkill, aimingMod, targetStanceMod, targetCoverMod, targetClose, spotterInjuryMod, shooterStanceMod, 
				shooterMiscMod, temp_elevation, vehiclesTargetMod, shootingVehicleMoving);
			TextView result = FindViewById<TextView> (Resource.Id.shootingValuesField);

			result.Text = calculatedValue.ToString() + Resources.GetString (Resource.String.end_shooting);

		}

		public void Spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{

		}
	}
		
}

