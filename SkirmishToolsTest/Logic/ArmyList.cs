using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
    class ArmyList
    {
        public Faction listFaction;
        public List<SoldierData> soldiers;

        public int GetPointsCost()
        {
            if(soldiers.Count < 1)
            {
                return 0;
            }

            int pointsCost = 0;

            foreach(SoldierData soldier in soldiers)
            {
                pointsCost += soldier.pointsCost;
            }

            // Add weapons teams, assets, etc

            return pointsCost;
        }
}
}