﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	public class CalculatorLogicV2
	{
		Dictionary<Modifiers, int> Values = new Dictionary<Modifiers, int> ();

		int currentValue;

		public CalculatorLogicV2 (Activity newContext, CalculatorType type)
		{
			switch (type) {
			case CalculatorType.Spotting:
				SetupSpotting (newContext);
				break;
			case CalculatorType.Shooting:
				SetupShooting (newContext);
				break;
			case CalculatorType.Morale:
				SetupMorale (newContext);
				break;
			case CalculatorType.Grenade:
				SetupGrenade (newContext);
				break;
			default:
				break;
			}
		}

		private void SetupSpotting (Activity newContext)
		{
			Values.Add(Modifiers.ElevationDifference, newContext.Resources.GetInteger(Resource.Integer.spot_elevationDifference));
			Values.Add(Modifiers.TargetCrawling, newContext.Resources.GetInteger(Resource.Integer.spot_target_crawl));
			Values.Add(Modifiers.TargetWalking, newContext.Resources.GetInteger(Resource.Integer.spot_target_walking));
			Values.Add(Modifiers.TargetRunning, newContext.Resources.GetInteger(Resource.Integer.spot_target_running));
			Values.Add(Modifiers.TargetProne, newContext.Resources.GetInteger(Resource.Integer.spot_target_prone));
			Values.Add(Modifiers.TargetKneeling, newContext.Resources.GetInteger(Resource.Integer.spot_target_kneeling));
			Values.Add(Modifiers.TargetUnderBarage, newContext.Resources.GetInteger(Resource.Integer.spot_target_underBarage));
			Values.Add(Modifiers.AdditionalTargets, newContext.Resources.GetInteger(Resource.Integer.spot_target_perAdditionalTarget));
			Values.Add(Modifiers.TargetFiringBoltActionRifle, newContext.Resources.GetInteger(Resource.Integer.spot_target_firingBolt));
			Values.Add(Modifiers.TargetFiringAutomaticWeapon, newContext.Resources.GetInteger(Resource.Integer.spot_target_firingAutomatic));
			Values.Add(Modifiers.TargetFiringHeavyWeapon, newContext.Resources.GetInteger(Resource.Integer.spot_target_firingHeavy));
			Values.Add(Modifiers.TargetInSoftCover, newContext.Resources.GetInteger(Resource.Integer.spot_target_softCover));
			Values.Add(Modifiers.TargetInMediumCover, newContext.Resources.GetInteger(Resource.Integer.spot_target_mediumCover));
			Values.Add(Modifiers.TargetInHardCover, newContext.Resources.GetInteger(Resource.Integer.spot_target_hardCover));
			Values.Add(Modifiers.TargetInPreparedDefences, newContext.Resources.GetInteger(Resource.Integer.spot_target_preparedDefences));
			Values.Add(Modifiers.TargetHidden, newContext.Resources.GetInteger(Resource.Integer.spot_target_hidden));
			Values.Add(Modifiers.TargetWearingGhilleSuit, newContext.Resources.GetInteger(Resource.Integer.spot_target_ghille));
			Values.Add(Modifiers.TargetUnderFire, newContext.Resources.GetInteger(Resource.Integer.spot_target_underFire));
			Values.Add(Modifiers.TimeIsDuskorDawn, newContext.Resources.GetInteger(Resource.Integer.spot_target_duskOrDawn));
			Values.Add(Modifiers.TimeIsNight, newContext.Resources.GetInteger(Resource.Integer.spot_target_night));

			Values.Add(Modifiers.ActiveSoldierUsingSniperScope, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_sniperScope));
			Values.Add(Modifiers.ActiveSoldierWithin12inchofExplosion, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_closeToExplo));
			Values.Add(Modifiers.ActiveSoldierUsingThermalOptics, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_thermalSight));
			Values.Add(Modifiers.ActiveSoldierUsingNVGs, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_nvg));
			Values.Add(Modifiers.ActiveSoldierWalking, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_walking));
			Values.Add(Modifiers.ActiveSoldierRunning, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_running));

			Values.Add(Modifiers.TargetVehicleMoving, newContext.Resources.GetInteger(Resource.Integer.spot_targetV_moving));
			Values.Add(Modifiers.TargetVehicleFiringSecondary, newContext.Resources.GetInteger(Resource.Integer.spot_targetV_firingSec));
			Values.Add(Modifiers.TargetVehicleFiringPrimary, newContext.Resources.GetInteger(Resource.Integer.spot_targetV_firingPri));
			Values.Add(Modifiers.TargetVehicleSideOn, newContext.Resources.GetInteger(Resource.Integer.spot_targetV_SideOn));
			Values.Add(Modifiers.TargetVehicleHardCover, newContext.Resources.GetInteger(Resource.Integer.spot_targetV_HardCover));
			Values.Add(Modifiers.TargetVehicleSoftCover, newContext.Resources.GetInteger(Resource.Integer.spot_targetV_SoftCover));
			Values.Add(Modifiers.TargetVehicleHullDown, newContext.Resources.GetInteger(Resource.Integer.spot_targetV_HullDown));
			Values.Add(Modifiers.ActiveVehicleButtonedUp, newContext.Resources.GetInteger(Resource.Integer.spot_spotterV_buttonedUp));
			Values.Add(Modifiers.ActiveVehicleMoving, newContext.Resources.GetInteger(Resource.Integer.spot_spotterV_Moving));

			Values.Add (Modifiers.None, 0);
		}

		private void SetupShooting (Activity newContext)
		{
			Values.Add(Modifiers.ElevationDifference, newContext.Resources.GetInteger(Resource.Integer.shoot_elevationDifference));
			Values.Add(Modifiers.TargetCrawling, newContext.Resources.GetInteger(Resource.Integer.shoot_target_walking));
			Values.Add(Modifiers.TargetWalking, newContext.Resources.GetInteger(Resource.Integer.shoot_target_walking));
			Values.Add(Modifiers.TargetRunning, newContext.Resources.GetInteger(Resource.Integer.shoot_target_running));
			Values.Add(Modifiers.TargetProne, newContext.Resources.GetInteger(Resource.Integer.shoot_target_prone));
			Values.Add(Modifiers.TargetKneeling, newContext.Resources.GetInteger(Resource.Integer.shoot_target_kneeling));
			Values.Add(Modifiers.TargetInSoftCover, newContext.Resources.GetInteger(Resource.Integer.shoot_target_softCover));
			Values.Add(Modifiers.TargetInMediumCover, newContext.Resources.GetInteger(Resource.Integer.shoot_target_mediumCover));
			Values.Add(Modifiers.TargetInHardCover, newContext.Resources.GetInteger(Resource.Integer.shoot_target_hardCover));
			Values.Add(Modifiers.TargetInPreparedDefences, newContext.Resources.GetInteger(Resource.Integer.shoot_target_preparedDefences));
			Values.Add (Modifiers.TargetIsClose, newContext.Resources.GetInteger (Resource.Integer.shoot_target_close));

			Values.Add (Modifiers.ActiveSoldierAiming1, newContext.Resources.GetInteger (Resource.Integer.shoot_target_aiming1));
			Values.Add (Modifiers.ActiveSoldierAiming2, newContext.Resources.GetInteger (Resource.Integer.shoot_target_aiming2));
			Values.Add(Modifiers.ActiveSoldierRunning, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_running));
			Values.Add(Modifiers.ActiveSoldierWalking, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_walking));
			Values.Add (Modifiers.ActiveSoldierCrawling, newContext.Resources.GetInteger (Resource.Integer.shoot_shooter_walking));
			Values.Add (Modifiers.ActiveSoldierKneeling, newContext.Resources.GetInteger (Resource.Integer.shoot_shooter_kneeling));
			Values.Add (Modifiers.ActiveSoldierProne, newContext.Resources.GetInteger (Resource.Integer.shoot_shooter_prone));
			Values.Add(Modifiers.ActiveSoldierUsingSniperScope, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_sniperScope));
			Values.Add (Modifiers.ActiveSoldierRidingAnimal, newContext.Resources.GetInteger (Resource.Integer.shoot_shooter_animal));
			Values.Add (Modifiers.ActiveSoldierSnapFire, newContext.Resources.GetInteger (Resource.Integer.shoot_shooter_snapFire));
			Values.Add (Modifiers.ActiveSoldierWeaponBraced, newContext.Resources.GetInteger (Resource.Integer.shoot_shooter_kneeling));

			Values.Add (Modifiers.TargetVehicleMoving, newContext.Resources.GetInteger (Resource.Integer.shoot_targetV_moving));
			Values.Add (Modifiers.TargetVehicleSoftCover, newContext.Resources.GetInteger (Resource.Integer.shoot_targetV_SoftCover));
			Values.Add (Modifiers.TargetVehicleHardCover, newContext.Resources.GetInteger (Resource.Integer.shoot_targetV_HardCover));
			Values.Add (Modifiers.ActiveVehicleMoving, newContext.Resources.GetInteger (Resource.Integer.shoot_shooterV_moving));

			Values.Add (Modifiers.None, 0);
		}

		private void SetupGrenade (Activity newContext)
		{
			Values.Add(Modifiers.TargetRunning, newContext.Resources.GetInteger(Resource.Integer.grenade_target_running));
			Values.Add (Modifiers.TargetVehicleMoving, newContext.Resources.GetInteger (Resource.Integer.grenade_target_movingVehicle));

			Values.Add(Modifiers.ActiveSoldierRunning, newContext.Resources.GetInteger(Resource.Integer.grenade_shooter_running));
			Values.Add(Modifiers.ActiveSoldierWalking, newContext.Resources.GetInteger(Resource.Integer.grenade_shooter_walking));
			Values.Add(Modifiers.ActiveSoldierCrawling, newContext.Resources.GetInteger(Resource.Integer.grenade_shooter_walking));
			Values.Add (Modifiers.ActiveVehicleMoving, newContext.Resources.GetInteger (Resource.Integer.grenade_shooter_movingVehicle));

			Values.Add(Modifiers.ElevationDifference, newContext.Resources.GetInteger(Resource.Integer.shoot_elevationDifference));
			Values.Add (Modifiers.None, 0);
		}

		private void SetupMorale (Activity newContext)
		{
			Values.Add (Modifiers.ActiveSoldierInflictedWounds,newContext.Resources.GetInteger(Resource.Integer.morale_inflictedWounds));
			Values.Add (Modifiers.MoraleConciousBuddies,newContext.Resources.GetInteger(Resource.Integer.morale_couciousSquadMember1));
			Values.Add (Modifiers.MoraleNearbyEnemies,newContext.Resources.GetInteger(Resource.Integer.morale_enemySquadWithin3));
			Values.Add (Modifiers.ShowOfForce,newContext.Resources.GetInteger(Resource.Integer.morale_showOfForce));
			Values.Add (Modifiers.MoraleUnderFireMG,newContext.Resources.GetInteger(Resource.Integer.morale_UnderFire_SniperMG));
			Values.Add (Modifiers.MoraleUnderFireExplo,newContext.Resources.GetInteger(Resource.Integer.morale_UnderFire_MortarRPG));
			Values.Add (Modifiers.MoraleUnderFireHeavy,newContext.Resources.GetInteger(Resource.Integer.morale_UnderFire_Heavy));
			Values.Add (Modifiers.MoraleAdditionalMoraleMarkers,newContext.Resources.GetInteger(Resource.Integer.morale_additionalMoraleMarker));
			Values.Add (Modifiers.TargetInSoftCover,newContext.Resources.GetInteger(Resource.Integer.morale_softCover));
			Values.Add (Modifiers.TargetInMediumCover,newContext.Resources.GetInteger(Resource.Integer.morale_mediumCover));
			Values.Add (Modifiers.TargetInHardCover,newContext.Resources.GetInteger(Resource.Integer.morale_hardCover));
			Values.Add (Modifiers.ActiveSoldierInVehicle,newContext.Resources.GetInteger(Resource.Integer.morale_inVehicle));
			Values.Add (Modifiers.MoraleInjuredNearby,newContext.Resources.GetInteger(Resource.Integer.morale_injuredFriendly));
			Values.Add (Modifiers.None, 0);
		}

		int GetElevationValue(enum_elevationDifferences elevation)
		{
			int elevationValue = 0;

			switch (elevation) {
			case enum_elevationDifferences.elevation_3higher:
				elevationValue = (-(Values [Modifiers.ElevationDifference] * 3));
				break;
			case enum_elevationDifferences.elevation_2higher:
				elevationValue = (-(Values [Modifiers.ElevationDifference] * 2));
				break;
			case enum_elevationDifferences.elevation_1higher:
				elevationValue = (-(Values [Modifiers.ElevationDifference] * 1));
				break;
			case enum_elevationDifferences.elevation_0higher:
				elevationValue = 0;
				break;
			case enum_elevationDifferences.elevation_1lower:
				elevationValue = (Values [Modifiers.ElevationDifference] * 1);
				break;
			case enum_elevationDifferences.elevation_2lower:
				elevationValue = (Values [Modifiers.ElevationDifference] * 2);
				break;
			case enum_elevationDifferences.elevation_3lower:
				elevationValue = (Values [Modifiers.ElevationDifference] * 3);
				break;
			}

			return elevationValue;
		}

		public int Calculate (List<Modifiers> modifiers)
		{
			int modifiersTotal = 0;

			if (modifiers.Count == 0) {
				return 0;
			}

			foreach (Modifiers mods in modifiers) {
				modifiersTotal += Values [mods];
			}

			return modifiersTotal;
		}

		public int CalculateSpotting (int startingValue, int additionalTargets, Modifiers enviormental, Modifiers targetStance,
									 Modifiers targetShooting, Modifiers targetCover, List<Modifiers> targetMisc, InjuryLevel spotterInjury, Modifiers spotterStance, List<Modifiers> spotterMisc,
									 enum_elevationDifferences elevation, List<Modifiers> vehiclesTarget, List<Modifiers> vehiclesSpotting)
		{
			// Create modifiers array
			currentValue = startingValue;

			switch (spotterInjury) {
			case InjuryLevel.Fine:
				break;
			case InjuryLevel.Light:
				currentValue -= 20;
				break;
			case InjuryLevel.Medium:
				currentValue = currentValue / 2;
				break;
			case InjuryLevel.Serious:
				currentValue = 10;
				break;
			}

			int elevationValue = GetElevationValue (elevation);

			currentValue += elevationValue;

			currentValue += (additionalTargets * Values [Modifiers.AdditionalTargets]);

			List<Modifiers> mainModifersList = new List<Modifiers> ();

			mainModifersList.Add(enviormental);
			mainModifersList.Add (targetStance);
			mainModifersList.Add (targetShooting);
			mainModifersList.Add (targetCover);
			mainModifersList.AddRange (targetMisc);
			mainModifersList.Add (spotterStance);
			mainModifersList.AddRange (spotterMisc);
			mainModifersList.AddRange (vehiclesTarget);
			mainModifersList.AddRange (vehiclesSpotting);

			int modifierValue = Calculate (mainModifersList);

			currentValue += modifierValue;


			return currentValue;
		}

		public int CalculateShooting (int startingValue, Modifiers aim, Modifiers targetStance,
			Modifiers targetCover, bool targetClose, InjuryLevel shooterInjury, Modifiers shooterStance, List<Modifiers> shooterMisc,
			enum_elevationDifferences elevation, List<Modifiers> vehiclesTarget, bool vehicleIsMoving)
		{
			// Create modifiers array
			currentValue = startingValue;

			switch (shooterInjury) {
			case InjuryLevel.Fine:
				break;
			case InjuryLevel.Light:
				currentValue -= 20;
				break;
			case InjuryLevel.Medium:
				currentValue = currentValue / 2;
				break;
			case InjuryLevel.Serious:
				currentValue = 10;
				break;
			}

			currentValue += GetElevationValue (elevation);

			List<Modifiers> mainModifersList = new List<Modifiers> ();

			mainModifersList.Add(aim);
			if (targetStance == Modifiers.TargetProne && targetClose) {
				// Do nothing - if prone and close nothing occurs
			} else {
				mainModifersList.Add (targetStance);
			}

			if (targetClose) {
				mainModifersList.Add (Modifiers.TargetIsClose);
			}
				
			mainModifersList.Add (targetCover);
			mainModifersList.Add (shooterStance);
			mainModifersList.AddRange (shooterMisc);
			mainModifersList.AddRange (vehiclesTarget);
			if (vehicleIsMoving) {
				mainModifersList.Add(Modifiers.ActiveVehicleMoving);
			}

			int modifierValue = Calculate (mainModifersList);

			currentValue += modifierValue;


			return currentValue;
		}

		public int CalculateGrenade (int startingValue, Modifiers targetStance, bool targetVehicleIsMoving,
			InjuryLevel shooterInjury, Modifiers shooterStance,
			enum_elevationDifferences elevation, bool shooterVehicleIsMoving)
		{
			// Create modifiers array
			currentValue = startingValue;

			switch (shooterInjury) {
			case InjuryLevel.Fine:
				break;
			case InjuryLevel.Light:
				currentValue -= 20;
				break;
			case InjuryLevel.Medium:
				currentValue = currentValue / 2;
				break;
			case InjuryLevel.Serious:
				currentValue = 10;
				break;
			}

			currentValue += GetElevationValue (elevation);

			List<Modifiers> mainModifersList = new List<Modifiers> ();

			mainModifersList.Add (targetStance);
		
			mainModifersList.Add (shooterStance);

			if (targetVehicleIsMoving) {
				mainModifersList.Add (Modifiers.TargetVehicleMoving);
			}
			if (shooterVehicleIsMoving) {
				mainModifersList.Add(Modifiers.ActiveVehicleMoving);
			}

			int modifierValue = Calculate (mainModifersList);

			currentValue += modifierValue;


			return currentValue;
		}

		public int CalculateMorale(int startingValue, int friendlies, int enemies, int moraleMarkers, Modifiers activeSoldierCover, List<Modifiers> activeSoldierDetails)
		{
			// Create modifiers array
			currentValue = startingValue;

			List<Modifiers> mainModifersList = new List<Modifiers> ();

			int friendliesValue = friendlies * Values [Modifiers.MoraleConciousBuddies];

			currentValue += friendliesValue;


			if (enemies > 1) {
				int enemiesValue = (enemies - 1) * Values [Modifiers.MoraleNearbyEnemies];

					currentValue += enemiesValue;
			}

			if (moraleMarkers > 1) {
				int moraleMarkersValue = (moraleMarkers - 1) * Values [Modifiers.MoraleAdditionalMoraleMarkers];

				currentValue += moraleMarkersValue;
			}

			mainModifersList.Add (activeSoldierCover);

			mainModifersList.AddRange (activeSoldierDetails);

			currentValue += Calculate (mainModifersList);

			return currentValue;
		}
	}
}

