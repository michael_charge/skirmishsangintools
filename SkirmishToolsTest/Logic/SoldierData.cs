﻿using System;
using System.Collections.Generic;


namespace SkirmishTools
{
	public class SoldierData
	{

        // Details
        public string Name;

        public SoldierType side;

        public ExperienceLevels experienceLevel;

        public UpgradePackagesInsurgents upgradeIrregular;
        public UpgradePackagesRegular upgradeRegular;

        public List<Weapons> weaponsList = new List<Weapons>();
        public int fragGrenades = 0;
        public int smokeGrenades = 0;
        public int stunGrenades = 0;

        public int barMines = 0; // Disallow unless engineer

        public BodyArmour armour;

        public int body = 0;

        public int pointsCost = 0;

        // Generated stats

        public int[] combatPhases = new int[4];
        public int modifiedBody;

        public int Pistol, Rifle, HeavyWeapon, Spot, FirstAid, Throw, FO;
        public int Engineering;
        public int morale;

        // Campaign stuff
        int battlesPlayed, battlesSurvived;

		public SoldierData ()
		{
            battlesPlayed = 0;
            battlesSurvived = 0;
		}
	}
}

