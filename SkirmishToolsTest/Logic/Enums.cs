﻿using System;

namespace SkirmishTools
{
	#region
	public enum Faction
	{
		None,
		GenericIrregular,
		GenericRegular,
		GenericMix,
		ISAF,
		Taliban,
		USArmy,
		USMarine,
		USSOCOM,
		USIntel,
		UKArmy,
		UKMarines,
		UKSF,
		FrenchArmy,
		FrenchSF,
		GermanArmy,
		GermanSF,
		CanadianArmy,
		CanadianSF,
		AusArmy,
		AusSF,
		NZArmy,
		NZSF,
		PMC,
		SomaliMilitia,
		AfricanMilita,
		AfricanArmy,
		MultinationalRegular,
	};

	public enum ExperienceLevels
	{
		Novice,
		Average,
		Veteran,
		Elite}

	;

	public enum SoldierType
	{
		Unknown,
		RegularForces,
		Insurgents,
		Militia}

	;

	public enum UpgradePackagesAll
	{
		None,
		Officer,
		NCO,
		Corporal,
		Medic,
		Sharpshooter,
		Sniper,
		Mujahedeen,
		AlQaeda,
		SwordOfAllah,
		WarriorIman}

	;

	// Individual Types
	public enum UpgradePackagesRegular
	{
		None,
		Officer,
		NCO,
		Corporal,
		Medic,
		Sharpshooter,
		Sniper,
		JTAC,
		CombatEngineer,
		DogHandler}

	;

	public enum UpgradePackagesInsurgents
	{
		None,
		Mujahedeen,
		AlQaeda,
		Sniper,
		WarriorIman}

	;

	public enum UpgradePackagesMilitia
	{
		None,
		MilitiaLeader
	}

	public enum Weapons
	{
		None,
		Pistol,
		AssaultRifle,
		DMR,
		SMG,
		CombatShotgun,
		SniperRifle,
		LMG,
		GPMG,
		AntiMaterialRifle,
		GrenadeLauncher,
		RPG,
		AT4,
		LAW,
		Panzerfaust3}

	;

	public enum BodyArmour
	{
		None,
		Light,
		Heavy}

	;

	#endregion

	public enum CloseCombatAttacks
	{
		StandardAttack,
		FeintandAttack,
		ChargeForwardandAttack,
		CircleAttack,
		Breakaway,
		Trip,
		AllOutAttack}

	;

	public enum InjuryLevel
	{
		Fine,
		Light,
		Medium,
		Serious,
		Critical,
		Dead}

	;

	// Weapon Teams


	// Assets

	public enum GrenadeModifiers
	{
		TargetRunning,
		TargetAboveShooter1,
		TargetAboveShooter2,
		TargetAboveShooter3,
		ShooterRunning,
		ShooterWalkingClimbingCrawlingSwimming,
		ShooterAboveTarget1,
		ShooterAboveTarget2,
		ShooterAboveTarget3,
	};

	public enum MoraleModifiers
	{
		InflictedWoundsInLastCombatPhase,
		OneConsciousSquadMemberwithin6inch,
		TwoConsciousSquadMemberwithin6inch,
		ThreeConsciousSquadMemberwithin6inch,
		ThreeEnemySquadMembersWithin6inch,
		FourEnemySquadMembersWithin6inch,
		FiveEnemySquadMembersWithin6inch,
		ShowofForce,
		UnderFirefromSnipersoranykindofMG,
		UnderFirefromMortarsorRPGs,
		UnderFire_Heavy,
		AdditionalMoraleMarkers,
		InSoftCover,
		InMediumCover,
		InHardCover,
		InVehicle,
		InjuredKilledFriendlieswithin12inchLOS,
	};

	public enum SpottingModifiers
	{
		TargetCrawlingClimbingSwimming,
		TargetWalking,
		TargetRunning,
		TargetProne,
		TargetKneeling,
		TargetUnderBarage,
		AdditionalTargets,
		TargetFiringBoltActionRifle,
		TargetFiringAutomaticWeapon,
		TargetFiringHeavyWeapon,
		TargetInSoftCover,
		TargetInMediumCover,
		TargetInHardCover,
		TargetInPreparedDefences,
		TargetHidden,
		TargetWearingGhilleSuit,
		AdditionalMoraleMarkersonTarget,
		TimeIsDuskorDawn,
		TimeIsNight,
		TargetAboveSpotter,
		TargetBelowSpotter,
		SpotterUsingSniperScope,
		SpotterWithin12inchofExplosion,
		SpotterUsingThermalOptics,
		SpotterUsingNVGs,
		SpotterWalking,
		SpotterRunning,
	};

	public enum ShootingModifiers
	{
		Using1APtoaim,
		Using2APtoaim,
		TargetRunning,
		TargetWalkingCrawlingClimbingSwimming,
		TargetKneeling,
		TargetProne,
		TargetInSoftCover,
		TargetInMediumCover,
		TargetInHardCover,
		TargetInPreparedDefences,
		TargetAboveShooter,
		Targetwithin4inch,
		ShooterRunning,
		ShooterWalkingClimbingCrawlingSwimming,
		ShooterKneelingorWeaponBraced,
		ShooterProne,
		ShooterAboveTarget,
		ShooterUsingSniperScope,
		ShooterRidingAnimal,
		ShooterSnapFiring,
	};

	// V2 Modifiers
	public enum Modifiers
	{
		None,
		TargetCrawling,
		TargetWalking,
		TargetRunning,
		TargetProne,
		TargetKneeling,
		TargetUnderBarage,
		TargetFiringBoltActionRifle,
		TargetFiringAutomaticWeapon,
		TargetFiringHeavyWeapon,
		TargetInSoftCover,
		TargetInMediumCover,
		TargetInHardCover,
		TargetInPreparedDefences,
		TargetHidden,
		TargetWearingGhilleSuit,
		AdditionalTargets,
		TargetIsClose,
		TargetUnderFire,
		TimeIsDuskorDawn,
		TimeIsNight,
		ElevationDifference,
		ActiveSoldierUsingSniperScope,
		ActiveSoldierWithin12inchofExplosion,
		ActiveSoldierUsingThermalOptics,
		ActiveSoldierUsingNVGs,
		ActiveSoldierWalking,
		ActiveSoldierRunning,
		ActiveSoldierCrawling,
		ActiveSoldierKneeling,
		ActiveSoldierProne,
		ActiveSoldierAiming1,
		ActiveSoldierAiming2,
		ActiveSoldierRidingAnimal,
		ActiveSoldierSnapFire,
		ActiveSoldierWeaponBraced,
		TargetVehicleMoving,
		TargetVehicleFiringSecondary,
		TargetVehicleFiringPrimary,
		TargetVehicleSideOn,
		TargetVehicleHardCover,
		TargetVehicleSoftCover,
		TargetVehicleHullDown,
		ActiveVehicleButtonedUp,
		ActiveVehicleMoving,
		ActiveSoldierInflictedWounds,
		MoraleConciousBuddies,
		MoraleNearbyEnemies,
		ShowOfForce,
		MoraleUnderFireMG,
		MoraleUnderFireExplo,
		MoraleUnderFireHeavy,
		MoraleAdditionalMoraleMarkers,
		ActiveSoldierInVehicle,
		MoraleInjuredNearby
	};

	public enum enum_elevationDifferences
	{
		elevation_3higher,
		elevation_2higher,
		elevation_1higher,
		elevation_0higher,
		elevation_1lower,
		elevation_2lower,
		elevation_3lower
	};

	public enum enum_Enviromental
	{
		Clear,
		DuskDawn,
		Night
	};

	public enum enum_Cover
	{
		None,
		Soft,
		Medium,
		Hard,
		Prepared
	};

	public enum enum_Stances
	{
		Standing,
		Running,
		Walking,
		Kneeling,
		Crawling,
		Prone
	};

	public enum enum_SpottingTargetShooting
	{
		NotShooting,
		ShootingBoltAction,
		ShootingAutomatic,
		ShootingHeavy
	}

	public enum enum_SpottingTargetMisc
	{
		UnderBarage,
		Hidden,
		GhilleSuit,
		UnderFire
	}

	public enum enum_SpottingSpotterMisc
	{
		SniperScope,
		CloseToExplosion,
		ThermalOptics,
		NVGs
	}

	public enum enum_SpottingTargetVMisc
	{
		VMoving,
		VFiringSecondary,
		VFiringPrimary,
		VSideOn,
		VHardCover,
		VSoftCover,
		VHullDown
	}

	public enum enum_SpottingSpottingVMisc
	{
		VButtonedUp,
		VMoving
	}

	public enum enum_ShootingShooterMisc
	{
		SSniperScope,
		SRidingAnimal,
		SSnapFire,
		SWeaponBraced
	}

	public enum enum_ShootingTargetV
	{
		TVMoving,
		TVHardCover,
		TVSoftCover
	}

	public enum enum_MoraleMisc
	{
		InflictedWounds,
		ShowOfForce,
		UnderFireMG,
		UnderFireExplo,
		UnderFireHeavy,
		InVehicle,
		InjuredFriendlies
	}
}