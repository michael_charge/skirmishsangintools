﻿using System;
using System.Collections.Generic;

namespace SkirmishTools
{
public enum AttributeType
    {
        Unknown,
        Side,
        ExperienceLevel,
        UpgradeIrregular,
        UpgradeRegular,
        Weapon,
        FragNumber,
        SmokeNumber,
        StunNumber,
        BarMineNumber,
        ArmourType,
        BodyValue
    };

    public class SoldierGeneration
    {
        SoldierData newSoldierData;

        public SoldierGeneration()
        {
            newSoldierData = new SoldierData();
        }

        public void Restart()
        {
            newSoldierData = null; // Reset to nothing then recreate
            newSoldierData = new SoldierData();
        }

        public void SetName(string _name)
        {
            newSoldierData.Name = _name;
        }

        public void SetAttribute(AttributeType _attributeType, int _attribute)
        {
            switch (_attributeType)
            {

                case AttributeType.Side:
                    newSoldierData.side = (SoldierType)_attribute;
                    break;
                case AttributeType.ExperienceLevel:
                    newSoldierData.experienceLevel = (ExperienceLevels)_attribute;
                    break;
                case AttributeType.UpgradeIrregular:
                    newSoldierData.upgradeIrregular = (UpgradePackagesInsurgents)_attribute;
                    break;
                case AttributeType.UpgradeRegular:
                    newSoldierData.upgradeRegular = (UpgradePackagesRegular)_attribute;
                    break;
                case AttributeType.Weapon:
                    if(_attribute > 0)
                    {
                        newSoldierData.weaponsList.Add((Weapons)_attribute);
                    }
                    break;
                case AttributeType.FragNumber:
                    newSoldierData.fragGrenades = _attribute;
                    break;
                case AttributeType.SmokeNumber:
                    newSoldierData.smokeGrenades = _attribute;
                    break;
                case AttributeType.StunNumber:
                    newSoldierData.stunGrenades = _attribute;
                    break;
                case AttributeType.BarMineNumber:
                    newSoldierData.barMines = _attribute;
                    break;
                case AttributeType.ArmourType:
                    newSoldierData.armour = (BodyArmour)_attribute;
                    break;
                case AttributeType.BodyValue:
                    newSoldierData.body = _attribute;
                    break;
                default:
                    break;
            }
        }

        public void Calculate()
        {
            GenerateSkills();
            CalculatePoints();
        }

        public bool GenerateSkills()
        {
            if (newSoldierData.side == SoldierType.Unknown || newSoldierData.body == 0)
            {
                return false;
            }

            // Make sure its within the correct values
            if (newSoldierData.body < 10 && newSoldierData.body > 0)
            {
                newSoldierData.body = 10;
            }
            else if (newSoldierData.body > 20)
            {
                newSoldierData.body = 20;
            }

            if (newSoldierData.armour != BodyArmour.None)
            {
                switch (newSoldierData.armour)
                {
                    case BodyArmour.Heavy:
                        newSoldierData.modifiedBody = newSoldierData.body - 2;
                        break;
                    case BodyArmour.Light:
                        newSoldierData.modifiedBody = newSoldierData.body - 1;
                        break;
                }
            }
            else
            {
                newSoldierData.modifiedBody = newSoldierData.body;
            }

            //  Get combat phases
            newSoldierData.combatPhases = Utilities.GetCombatPhases(newSoldierData.modifiedBody);

            // Generate stats
            int skillLevelModifier = Utilities.GetExperienceModifier(newSoldierData.experienceLevel);

            int basicSkill = skillLevelModifier * newSoldierData.body;

            newSoldierData.Pistol = basicSkill;
            newSoldierData.Rifle = basicSkill + 10;
            newSoldierData.HeavyWeapon = basicSkill - newSoldierData.body;
            newSoldierData.Spot = 100;
            newSoldierData.FirstAid = 40;
            newSoldierData.Throw = basicSkill;
            newSoldierData.FO = basicSkill - newSoldierData.body;
            newSoldierData.Engineering = 0;

            newSoldierData.morale = Utilities.GetUnModifiedMorale(newSoldierData.experienceLevel);

            #region Add upgrade packages
            if (newSoldierData.side == SoldierType.RegularForces)
            {
                switch (newSoldierData.upgradeRegular)
                {
                    case UpgradePackagesRegular.Officer:
                        newSoldierData.morale += 20;
                        newSoldierData.FO += 20;
                        break;
                    case UpgradePackagesRegular.NCO:
                        newSoldierData.morale += 10;
                        newSoldierData.FO += 10;
                        newSoldierData.Rifle += 10;
                        break;
                    case UpgradePackagesRegular.Corporal:
                        newSoldierData.FO += 10;
                        newSoldierData.Rifle += 10;
                        break;
                    case UpgradePackagesRegular.Medic:
                        newSoldierData.FirstAid += 40;
                        newSoldierData.morale += 20;
                        break;
                    case UpgradePackagesRegular.Sharpshooter:
                        newSoldierData.Rifle += 20;
                        newSoldierData.morale += 20;
                        break;
                    case UpgradePackagesRegular.Sniper:
                        newSoldierData.Rifle += 40;
                        newSoldierData.morale += 20;
                        break;
                    case UpgradePackagesRegular.JTAC:
                        newSoldierData.FO += 30;
                        break;
                    case UpgradePackagesRegular.CombatEngineer:
                        newSoldierData.Engineering = basicSkill + 20;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (newSoldierData.upgradeIrregular)
                {
                    case UpgradePackagesInsurgents.Mujahedeen:
                        newSoldierData.Rifle += 10;
                        newSoldierData.FO += 20;
                        newSoldierData.morale += 20;
                        break;
                    case UpgradePackagesInsurgents.AlQaeda:
                        newSoldierData.Rifle += 10;
                        newSoldierData.morale += 30;
                        break;
                    case UpgradePackagesInsurgents.Sniper:
                        newSoldierData.Rifle += 30;
                        newSoldierData.FO += 30;
                        break;
                    case UpgradePackagesInsurgents.WarriorIman:
					newSoldierData.morale += 20;
					newSoldierData.FO += 20;
                        break;
                    default:
                        break;
                }
            }
            #endregion


            return true;
        }

        public int CalculatePoints()
        {
            newSoldierData.pointsCost = Utilities.GetPoints(newSoldierData.experienceLevel);

            newSoldierData.pointsCost += Utilities.GetPoints(newSoldierData.armour);

            if (newSoldierData.side == SoldierType.RegularForces)
            {
                newSoldierData.pointsCost += newSoldierData.upgradeRegular == UpgradePackagesRegular.DogHandler ? Utilities.GetPoints(newSoldierData.experienceLevel) : Utilities.GetPoints(newSoldierData.upgradeRegular);
            }
            else
            {
                newSoldierData.pointsCost += Utilities.GetPoints(newSoldierData.upgradeIrregular);
            }

            foreach (Weapons weap in newSoldierData.weaponsList)
            {
                newSoldierData.pointsCost += Utilities.GetPoints(weap);
            }

            newSoldierData.pointsCost += 10 * newSoldierData.fragGrenades;
            newSoldierData.pointsCost += 10 * newSoldierData.smokeGrenades;
            newSoldierData.pointsCost += 10 * newSoldierData.stunGrenades;
            if (newSoldierData.upgradeRegular == UpgradePackagesRegular.CombatEngineer)
            {
                newSoldierData.pointsCost += 10 * newSoldierData.barMines;
            }

            return newSoldierData.pointsCost;
        }

        public SoldierData CreateNewSoldier()
        {

            return newSoldierData;
        }
    }
}