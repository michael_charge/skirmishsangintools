﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	public class CalculatorLogic
	{
		Dictionary<int, int> Values = new Dictionary<int, int>();

		int currentValue;

		public CalculatorLogic (Activity newContext, CalculatorType type)
		{

            switch (type)
            {
                case CalculatorType.Spotting:
                    SetupSpotting(newContext);
                    break;
                case CalculatorType.Shooting:
                    SetupShooting(newContext);
                    break;
                case CalculatorType.Morale:
                    SetupMorale(newContext);
                    break;
                case CalculatorType.Grenade:
                    SetupGrenade(newContext);
                    break;
                default:
                    break;
            }
        }

        private void SetupGrenade(Activity newContext)
        {
            Values.Add((int)GrenadeModifiers.TargetRunning, newContext.Resources.GetInteger(Resource.Integer.grenade_target_running));
			Values.Add((int) GrenadeModifiers.TargetAboveShooter1, newContext.Resources.GetInteger(Resource.Integer.grenade_target_perStoreyAbove1));
            Values.Add((int)GrenadeModifiers.TargetAboveShooter2, newContext.Resources.GetInteger(Resource.Integer.grenade_target_perStoreyAbove2));
            Values.Add((int)GrenadeModifiers.TargetAboveShooter3, newContext.Resources.GetInteger(Resource.Integer.grenade_target_perStoreyAbove3));
			Values.Add((int) GrenadeModifiers.ShooterRunning, newContext.Resources.GetInteger(Resource.Integer.grenade_shooter_running));
			Values.Add((int) GrenadeModifiers.ShooterWalkingClimbingCrawlingSwimming, newContext.Resources.GetInteger(Resource.Integer.grenade_shooter_walking));
			Values.Add((int) GrenadeModifiers.ShooterAboveTarget1, newContext.Resources.GetInteger(Resource.Integer.grenade_shooter_perStoreyAbove1));
			Values.Add((int) GrenadeModifiers.ShooterAboveTarget2, newContext.Resources.GetInteger(Resource.Integer.grenade_shooter_perStoreyAbove2));
			Values.Add((int) GrenadeModifiers.ShooterAboveTarget3, newContext.Resources.GetInteger(Resource.Integer.grenade_shooter_perStoreyAbove3));
        }

        private void SetupMorale(Activity newContext)
        {
			Values.Add((int) MoraleModifiers.InflictedWoundsInLastCombatPhase, newContext.Resources.GetInteger(Resource.Integer.morale_inflictedWounds));
			Values.Add((int) MoraleModifiers.OneConsciousSquadMemberwithin6inch, newContext.Resources.GetInteger(Resource.Integer.morale_couciousSquadMember1));
			Values.Add((int) MoraleModifiers.TwoConsciousSquadMemberwithin6inch, newContext.Resources.GetInteger(Resource.Integer.morale_couciousSquadMember2));
			Values.Add((int) MoraleModifiers.ThreeConsciousSquadMemberwithin6inch, newContext.Resources.GetInteger(Resource.Integer.morale_couciousSquadMember3));
			Values.Add((int) MoraleModifiers.ThreeEnemySquadMembersWithin6inch, newContext.Resources.GetInteger(Resource.Integer.morale_enemySquadWithin3));
			Values.Add((int) MoraleModifiers.FourEnemySquadMembersWithin6inch, newContext.Resources.GetInteger(Resource.Integer.morale_enemySquadWithin4));
			Values.Add((int) MoraleModifiers.FiveEnemySquadMembersWithin6inch, newContext.Resources.GetInteger(Resource.Integer.morale_enemySquadWithin5));
			Values.Add((int) MoraleModifiers.ShowofForce, newContext.Resources.GetInteger(Resource.Integer.morale_showOfForce));
			Values.Add((int) MoraleModifiers.UnderFirefromSnipersoranykindofMG, newContext.Resources.GetInteger(Resource.Integer.morale_UnderFire_SniperMG));
			Values.Add((int) MoraleModifiers.UnderFirefromMortarsorRPGs, newContext.Resources.GetInteger(Resource.Integer.morale_UnderFire_MortarRPG));
			Values.Add((int) MoraleModifiers.UnderFire_Heavy, newContext.Resources.GetInteger(Resource.Integer.morale_UnderFire_Heavy));
			Values.Add((int) MoraleModifiers.AdditionalMoraleMarkers, newContext.Resources.GetInteger(Resource.Integer.morale_additionalMoraleMarker));
			Values.Add((int) MoraleModifiers.InSoftCover, newContext.Resources.GetInteger(Resource.Integer.morale_softCover));
			Values.Add((int) MoraleModifiers.InMediumCover, newContext.Resources.GetInteger(Resource.Integer.morale_mediumCover));
			Values.Add((int) MoraleModifiers.InHardCover, newContext.Resources.GetInteger(Resource.Integer.morale_hardCover));
			Values.Add((int) MoraleModifiers.InVehicle, newContext.Resources.GetInteger(Resource.Integer.morale_inVehicle));
			Values.Add((int) MoraleModifiers.InjuredKilledFriendlieswithin12inchLOS, newContext.Resources.GetInteger(Resource.Integer.morale_injuredFriendly));
        }

        private void SetupShooting(Activity newContext)
        {
			Values.Add((int) ShootingModifiers.Using1APtoaim, newContext.Resources.GetInteger(Resource.Integer.shoot_target_aiming1));
			Values.Add((int) ShootingModifiers.Using2APtoaim, newContext.Resources.GetInteger(Resource.Integer.shoot_target_aiming2));
			Values.Add((int) ShootingModifiers.TargetRunning, newContext.Resources.GetInteger(Resource.Integer.shoot_target_running));
			Values.Add((int) ShootingModifiers.TargetWalkingCrawlingClimbingSwimming, newContext.Resources.GetInteger(Resource.Integer.shoot_target_walking));
			Values.Add((int) ShootingModifiers.TargetKneeling, newContext.Resources.GetInteger(Resource.Integer.shoot_target_kneeling));
			Values.Add((int) ShootingModifiers.TargetProne, newContext.Resources.GetInteger(Resource.Integer.shoot_target_prone));
			Values.Add((int) ShootingModifiers.TargetInSoftCover, newContext.Resources.GetInteger(Resource.Integer.shoot_target_softCover));
			Values.Add((int) ShootingModifiers.TargetInMediumCover, newContext.Resources.GetInteger(Resource.Integer.shoot_target_mediumCover));
			Values.Add((int) ShootingModifiers.TargetInHardCover, newContext.Resources.GetInteger(Resource.Integer.shoot_target_hardCover));
			Values.Add((int) ShootingModifiers.TargetInPreparedDefences, newContext.Resources.GetInteger(Resource.Integer.shoot_target_preparedDefences));
			Values.Add((int) ShootingModifiers.TargetAboveShooter, newContext.Resources.GetInteger(Resource.Integer.shoot_target_perStoreyAbove));
			Values.Add((int) ShootingModifiers.Targetwithin4inch, newContext.Resources.GetInteger(Resource.Integer.shoot_target_close));
			Values.Add((int) ShootingModifiers.ShooterRunning, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_running));
			Values.Add((int) ShootingModifiers.ShooterWalkingClimbingCrawlingSwimming, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_walking));
			Values.Add((int) ShootingModifiers.ShooterKneelingorWeaponBraced, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_kneeling));
			Values.Add((int) ShootingModifiers.ShooterProne, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_prone));
			Values.Add((int) ShootingModifiers.ShooterAboveTarget, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_perStoreyBelow));
			Values.Add((int) ShootingModifiers.ShooterUsingSniperScope, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_sniperScope));
			Values.Add((int) ShootingModifiers.ShooterRidingAnimal, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_animal));
			Values.Add((int) ShootingModifiers.ShooterSnapFiring, newContext.Resources.GetInteger(Resource.Integer.shoot_shooter_snapFire));
        }

        private void SetupSpotting(Activity newContext)
        {
			Values.Add((int) SpottingModifiers.TargetCrawlingClimbingSwimming, newContext.Resources.GetInteger(Resource.Integer.spot_target_crawl));
			Values.Add((int) SpottingModifiers.TargetWalking, newContext.Resources.GetInteger(Resource.Integer.spot_target_walking));
			Values.Add((int) SpottingModifiers.TargetRunning, newContext.Resources.GetInteger(Resource.Integer.spot_target_running));
			Values.Add((int) SpottingModifiers.TargetProne, newContext.Resources.GetInteger(Resource.Integer.spot_target_prone));
			Values.Add((int) SpottingModifiers.TargetKneeling, newContext.Resources.GetInteger(Resource.Integer.spot_target_kneeling));
			Values.Add((int) SpottingModifiers.TargetUnderBarage, newContext.Resources.GetInteger(Resource.Integer.spot_target_underBarage));
			Values.Add((int) SpottingModifiers.AdditionalTargets, newContext.Resources.GetInteger(Resource.Integer.spot_target_perAdditionalTarget));
			Values.Add((int) SpottingModifiers.TargetFiringBoltActionRifle, newContext.Resources.GetInteger(Resource.Integer.spot_target_firingBolt));
			Values.Add((int) SpottingModifiers.TargetFiringAutomaticWeapon, newContext.Resources.GetInteger(Resource.Integer.spot_target_firingAutomatic));
			Values.Add((int) SpottingModifiers.TargetFiringHeavyWeapon, newContext.Resources.GetInteger(Resource.Integer.spot_target_firingHeavy));
			Values.Add((int) SpottingModifiers.TargetInSoftCover, newContext.Resources.GetInteger(Resource.Integer.spot_target_softCover));
			Values.Add((int) SpottingModifiers.TargetInMediumCover, newContext.Resources.GetInteger(Resource.Integer.spot_target_mediumCover));
			Values.Add((int) SpottingModifiers.TargetInHardCover, newContext.Resources.GetInteger(Resource.Integer.spot_target_hardCover));
			Values.Add((int) SpottingModifiers.TargetInPreparedDefences, newContext.Resources.GetInteger(Resource.Integer.spot_target_preparedDefences));
			Values.Add((int) SpottingModifiers.TargetHidden, newContext.Resources.GetInteger(Resource.Integer.spot_target_hidden));
			Values.Add((int) SpottingModifiers.TargetWearingGhilleSuit, newContext.Resources.GetInteger(Resource.Integer.spot_target_ghille));
//			Values.Add((int) SpottingModifiers.AdditionalMoraleMarkersonTarget, newContext.Resources.GetInteger(Resource.Integer.spot_target_perMoraleMarker));
			Values.Add((int) SpottingModifiers.TimeIsDuskorDawn, newContext.Resources.GetInteger(Resource.Integer.spot_target_duskOrDawn));
			Values.Add((int) SpottingModifiers.TimeIsNight, newContext.Resources.GetInteger(Resource.Integer.spot_target_night));
			Values.Add((int) SpottingModifiers.SpotterUsingSniperScope, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_sniperScope));
//			Values.Add((int) SpottingModifiers.TargetAboveSpotter, newContext.Resources.GetInteger(Resource.Integer.spot_target_perStoreyAbove));
//			Values.Add((int) SpottingModifiers.TargetBelowSpotter, newContext.Resources.GetInteger(Resource.Integer.spot_target_perStoreyBelow));
			Values.Add((int) SpottingModifiers.SpotterWithin12inchofExplosion, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_closeToExplo));
			Values.Add((int) SpottingModifiers.SpotterUsingThermalOptics, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_thermalSight));
			Values.Add((int) SpottingModifiers.SpotterUsingNVGs, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_nvg));
			Values.Add((int) SpottingModifiers.SpotterWalking, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_walking));
			Values.Add((int) SpottingModifiers.SpotterRunning, newContext.Resources.GetInteger(Resource.Integer.spot_spotter_running));
        }

		public virtual int Calculate(List<int> checkedBoxes, int startingValue, int pointsOfDamage)
		{
			int customisedValue = startingValue;

			if (pointsOfDamage > 0) {
				InjuryLevel injury = Utilities.GetInjuryLevel (pointsOfDamage);

				switch (injury) {
				default:
					break;
				}
			}

			currentValue = customisedValue; 

			int outputValue;

			foreach (int value in checkedBoxes) {
				if(Values.TryGetValue(value, out outputValue))
				{
					currentValue += outputValue;
				}
			}

			return currentValue;
		}
	}
}

