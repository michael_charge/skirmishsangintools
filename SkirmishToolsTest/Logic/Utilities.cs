﻿using System;

namespace SkirmishTools
{
	public class Utilities
	{
			public static InjuryLevel GetInjuryLevel(int pointsOfDamage)
			{
				if (pointsOfDamage == 0) {
					return InjuryLevel.Fine;
				} else if (pointsOfDamage < 4 && pointsOfDamage > 0) {
					return InjuryLevel.Light;
				} else if (pointsOfDamage > 3 && pointsOfDamage < 7) {
					return InjuryLevel.Medium;
				} else if (pointsOfDamage < 10 && pointsOfDamage > 6) {
					return InjuryLevel.Serious;
				} else if (pointsOfDamage < 13 && pointsOfDamage > 11) {
					return InjuryLevel.Critical;
				} else {
					return InjuryLevel.Dead;
				}

			}

			public static int GetExperienceModifier (ExperienceLevels currentLevel)
			{
				switch (currentLevel) {
				case ExperienceLevels.Novice:
					return 2;
				case ExperienceLevels.Average:
					return 3;
				case ExperienceLevels.Veteran:
					return 4;
				case ExperienceLevels.Elite:
					return 5;
				default:
					return 1;
				}
			}

			public static int GetUnModifiedMorale (ExperienceLevels currentLevel)
			{
				switch (currentLevel) {
				case ExperienceLevels.Novice:
					return 35;
				case ExperienceLevels.Average:
					return 55;
				case ExperienceLevels.Veteran:
					return 75;
				case ExperienceLevels.Elite:
					return 90;
				default:
					return 55;
				}

			}

			public static int[] GetCombatPhases (int bodyValue)
			{
				if (bodyValue >= 18) {
					return new int[4] { 1, 3, 5, 7 };
				} else if (bodyValue >= 15 && bodyValue <= 17) {
					return new int[4] { 2, 4, 6, 8 };
				} else if (bodyValue >= 12 && bodyValue <= 14) {
					return new int[4] { 3, 5, 7, 9 };
				} else {
					return new int[4] { 4, 6, 8, 10 };
				}

			}

			#region GetPoints

			public static int GetPoints (ExperienceLevels experience)
			{
				switch (experience) {
				case ExperienceLevels.Novice:
					return 25;
				case ExperienceLevels.Average:
					return 50;
				case ExperienceLevels.Veteran:
					return 75;
				case ExperienceLevels.Elite:
					return 100;
				default:
					return 0;
				}
			}

			public static int GetPoints (UpgradePackagesRegular packageReg)
			{
				switch (packageReg) {
				case UpgradePackagesRegular.Officer:
					return 40;
				case UpgradePackagesRegular.NCO:
					return 30;
				case UpgradePackagesRegular.Corporal:
					return 20;
				case UpgradePackagesRegular.Medic:
					return 60;
				case UpgradePackagesRegular.Sharpshooter:
					return 40;
				case UpgradePackagesRegular.Sniper:
					return 80;
				case UpgradePackagesRegular.CombatEngineer:
					return 50;
				case UpgradePackagesRegular.JTAC:
					return 125;
				case UpgradePackagesRegular.DogHandler:
					return 0;
				default:
					return 0;
				}
			}

			public static int GetPoints (UpgradePackagesInsurgents packageIreg)
			{
				switch (packageIreg) {
				case UpgradePackagesInsurgents.Mujahedeen:
					return 50;
				case UpgradePackagesInsurgents.AlQaeda:
					return 30;
				case UpgradePackagesInsurgents.Sniper:
					return 60;
				case UpgradePackagesInsurgents.WarriorIman:
					return 40;
				default:
					return 0;
				}
			}

			public static int GetPoints (BodyArmour armour)
			{
				switch (armour) {
				case BodyArmour.Light:
					return 5;
				case BodyArmour.Heavy:
					return 10;
				default:
					return 0;
				}
			}

			public static int GetPoints (Weapons weapon)
			{
				switch (weapon) {
				case Weapons.Pistol:
					return 5;
				case Weapons.AssaultRifle:
					return 10;
				case Weapons.DMR:
					return 15;
				case Weapons.SMG:
					return 10;
				case Weapons.CombatShotgun:
					return 10;
				case Weapons.SniperRifle:
					return 10;
				case Weapons.LMG:
					return 25;
				case Weapons.GPMG:
					return 25;
				case Weapons.AntiMaterialRifle:
					return 25;
				case Weapons.GrenadeLauncher:
					return 25;
				case Weapons.RPG:
					return 25;
				case Weapons.AT4:
					return 25;
				default:
					return 0;
				}
			}

			public static int GetCombatPercentage (ExperienceLevels attackerExp, ExperienceLevels defenderExp)
			{
				switch (attackerExp) {
				case ExperienceLevels.Novice:
					switch (defenderExp) {
					case ExperienceLevels.Novice:

						return 60;

					case ExperienceLevels.Average:
						return 50;

					case ExperienceLevels.Veteran:
						return 40;

					case ExperienceLevels.Elite:
						return 30;
					}
					return 0;
				case ExperienceLevels.Average:
					switch (defenderExp) {
					case ExperienceLevels.Novice:

						return 70;

					case ExperienceLevels.Average:
						return 60;

					case ExperienceLevels.Veteran:
						return 50;

					case ExperienceLevels.Elite:
						return 40;
					}
					return 0;
				case ExperienceLevels.Veteran:
					switch (defenderExp) {
					case ExperienceLevels.Novice:

						return 80;

					case ExperienceLevels.Average:
						return 70;

					case ExperienceLevels.Veteran:
						return 60;

					case ExperienceLevels.Elite:
						return 50;
					}
					return 0;
				case ExperienceLevels.Elite:
					switch (defenderExp) {
					case ExperienceLevels.Novice:

						return 90;

					case ExperienceLevels.Average:
						return 80;

					case ExperienceLevels.Veteran:
						return 70;

					case ExperienceLevels.Elite:
						return 60;
					}
					return 0;

				default:
					return 0;
				}

			}

			#endregion
	}
}

