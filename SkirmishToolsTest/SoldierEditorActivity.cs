﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	[Activity (Label = "Skills Calculator")]			
	public class SoldierEditorActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);


			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.SoldierEditor);

			Spinner spinner = FindViewById<Spinner> (Resource.Id.experience_spinner);

			spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (experienceSpinner_ItemSelected);
			var adapter = ArrayAdapter.CreateFromResource (
				              this, Resource.Array.experienceLevel, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinner.Adapter = adapter;

			spinner = FindViewById<Spinner> (Resource.Id.experience_pack_spinner);

			spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (experienceSpinner_ItemSelected);
			var adapter2 = ArrayAdapter.CreateFromResource (
				               this, Resource.Array.experiencePackage, Android.Resource.Layout.SimpleSpinnerItem);

			adapter2.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinner.Adapter = adapter2;

			Button button = FindViewById<Button> (Resource.Id.calculateCharacterButton);

			button.Click += delegate {
				Calculate();
			};

		}

		// Event handlers
		private void experienceSpinner_ItemSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{

		}

		void Calculate ()
		{

			EditText bodyInput = FindViewById<EditText> (Resource.Id.characterBodyValue);

			if (bodyInput.Text != "") {

				// Get button value
				int bodyValue = Convert.ToInt32 (bodyInput.Text);

				// Check body is valid
				if (bodyValue < 10 && bodyValue > 0) {
					bodyValue = 10;
				} else if (bodyValue > 20) {
					bodyValue = 20;
				}

				// Get experience level and experience package
				Spinner spinner = FindViewById<Spinner> (Resource.Id.experience_spinner);

				ExperienceLevels experienceLevel = (ExperienceLevels)spinner.SelectedItemPosition;

				spinner = FindViewById<Spinner> (Resource.Id.experience_pack_spinner);

				UpgradePackagesAll packages = (UpgradePackagesAll)spinner.SelectedItemPosition;

				int Pistol, Rifle, HeavyWeapon, Spot, FirstAid, Throw, FO, morale;

				int skillLevelModifier = Utilities.GetExperienceModifier (experienceLevel);

				int basicSkill = skillLevelModifier * bodyValue;

				Pistol = basicSkill;
				Rifle = basicSkill + 10;
				HeavyWeapon = basicSkill - bodyValue;
				Spot = 100;
				FirstAid = 40;
				Throw = basicSkill;
				FO = basicSkill - bodyValue;

				morale = Utilities.GetUnModifiedMorale (experienceLevel);

				switch (packages) {
				case UpgradePackagesAll.Officer:
					morale += 20;
					FO += 20;
					break;
				case UpgradePackagesAll.NCO:
					morale += 10;
					FO += 10;
					Rifle += 10;
					break;
				case UpgradePackagesAll.Corporal:
					FO += 10;
					Rifle += 10;
					break;
				case UpgradePackagesAll.Medic:
					FirstAid += 40;
					morale += 20;
					break;
				case UpgradePackagesAll.Sharpshooter:
					Rifle += 20;
					morale += 20;
					break;
				case UpgradePackagesAll.Sniper:
					Rifle += 40;
					morale += 20;
					break;
				case UpgradePackagesAll.Mujahedeen:
					Rifle += 10;
					FO += 20;
					morale += 20;
					break;
				case UpgradePackagesAll.AlQaeda:
					Rifle += 10;
					morale += 30;
					break;
				case UpgradePackagesAll.SwordOfAllah:
					Rifle += 30;
					FO += 30;
					break;
				case UpgradePackagesAll.WarriorIman:
					morale += 20;
					FO += 20;
					break;
				default:
					break;
				}

				// output all data
				TextView text = FindViewById<TextView>(Resource.Id.pistolSkill);
				text.Text = GetString (Resource.String.pistolSkill) + " " + Pistol.ToString () + "%";
				text = FindViewById<TextView>(Resource.Id.rifleSkill);
				text.Text = GetString (Resource.String.rifleSkill) + " " + Rifle.ToString () + "%";
				text = FindViewById<TextView>(Resource.Id.spotSkill);
				text.Text = GetString (Resource.String.spotSkill) + " " + Spot.ToString () + "%";
				text = FindViewById<TextView>(Resource.Id.firstAid);
				text.Text = GetString (Resource.String.firstAidSkill) + " " + FirstAid.ToString () + "%";
				text = FindViewById<TextView>(Resource.Id.HeavyWeapon);
				text.Text = GetString (Resource.String.hWeapSkill) + " " + HeavyWeapon.ToString () + "%";
				text = FindViewById<TextView>(Resource.Id.throwSkill);
				text.Text = GetString (Resource.String.throwSkill) + " " + Throw.ToString () + "%";
				text = FindViewById<TextView>(Resource.Id.foSkill);
				text.Text = GetString (Resource.String.fOSkill) + " " + FO.ToString () + "%";
				text = FindViewById<TextView>(Resource.Id.moraleSkill);
				text.Text = GetString (Resource.String.moraleSkill) + " " + morale.ToString () + "%";
			} else {
				// TOAST NO INTERACTION

			}
		}
	}
}

