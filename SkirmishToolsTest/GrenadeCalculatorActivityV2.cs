
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	[Activity (Label = "Grenade Calculator", Icon = "@drawable/ic_action_soldier")]
	public class GrenadeCalculatorV2Activity : Activity
	{
		//SpottingCalculator spottingCalculator;
		CalculatorLogicV2 calculatorLogic;

		Spinner targetStance, shooterHealth, shooterStance, elevationDifference;
		CheckBox targetVehicleMovingBox, shooterVehicleMovingBox;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.GrenadeCalculatorV2);

			calculatorLogic = new CalculatorLogicV2 (this, CalculatorType.Grenade);


			//Target Stance
			targetStance = FindViewById<Spinner> (Resource.Id.TargetStanceSpinner);

			targetStance.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			ArrayAdapter adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Stances, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			targetStance.Adapter = adapter;

			targetVehicleMovingBox = FindViewById<CheckBox> (Resource.Id.targetVehicleMovingCheckbox);

			//Shooter Health
			shooterHealth = FindViewById<Spinner> (Resource.Id.ShooterHealthSpinner);

			shooterHealth.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.healthStates, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			shooterHealth.Adapter = adapter;

			//Shooter Stance
			shooterStance = FindViewById<Spinner> (Resource.Id.ShooterStanceSpinner);

			shooterStance.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Stances, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			shooterStance.Adapter = adapter;

			//Height Difference
			elevationDifference = FindViewById<Spinner> (Resource.Id.HeightDifferenceSpinner);

			elevationDifference.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Elevation, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);

			elevationDifference.Adapter = adapter;
			elevationDifference.SetSelection (3);

			//spottingVehicle
			shooterVehicleMovingBox = FindViewById<CheckBox> (Resource.Id.shootingVehicleMovingCheckbox);

			// Calculate button
			Button pushButton = FindViewById<Button> (Resource.Id.spotCalculate);
			pushButton.Click += delegate {
				Calculate();
			};

		}

		public void Calculate ()
		{
			int shootingSkill;

			bool validShootingSkill = int.TryParse((FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text), out shootingSkill);

			if (!validShootingSkill) {
				shootingSkill = 22;
				FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text = "22";
			} else {
				if (shootingSkill < 22) {
					shootingSkill = 22;
					FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text = "22";
				} else if (shootingSkill > 150) {
					shootingSkill = 150;
					FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text = "150";
				}
			}

			// target stance
			enum_Stances temp_targetStance = (enum_Stances)targetStance.SelectedItemPosition;
			Modifiers targetStanceMod = Modifiers.None;

			switch (temp_targetStance)
			{
				case enum_Stances.Running:
					targetStanceMod = Modifiers.TargetRunning;
					break;
			default:
				targetStanceMod = Modifiers.None;
					break;
			}

			// Target at close range
			bool targetVehicleMoving = targetVehicleMovingBox.Checked;

			// spotterInjury
			InjuryLevel spotterInjuryMod = (InjuryLevel) shooterHealth.SelectedItemPosition;


			// shooterStance
			enum_Stances temp_shooterStance = (enum_Stances) shooterStance.SelectedItemPosition;
			Modifiers shooterStanceMod = Modifiers.None;

			switch (temp_shooterStance)
			{
				case enum_Stances.Running:
					shooterStanceMod = Modifiers.ActiveSoldierRunning;
					break;
			default:
				shooterStanceMod = Modifiers.None;
					break;
			}

			// elevationDifferenes
			enum_elevationDifferences temp_elevation = (enum_elevationDifferences) elevationDifference.SelectedItemPosition;
				

			bool shootingVehicleMoving = shooterVehicleMovingBox.Checked;

			int calculatedValue = calculatorLogic.CalculateGrenade (shootingSkill, targetStanceMod, targetVehicleMoving, spotterInjuryMod, shooterStanceMod,
				                      temp_elevation, shootingVehicleMoving);
			TextView result = FindViewById<TextView> (Resource.Id.shootingValuesField);

			result.Text = calculatedValue.ToString() + Resources.GetString (Resource.String.end_shooting);

		}

		public void Spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{

		}
	}
		
}

