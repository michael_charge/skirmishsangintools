﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	[Activity (Label = "Close Combat Calculator")]			
	public class CloseCombatCalculatorActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.CloseCombatCalc);

			Spinner spinner = FindViewById<Spinner> (Resource.Id.attackerSpinner);

			//spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (experienceSpinner_ItemSelected);
			var adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.attacker_experienceLevel, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinner.Adapter = adapter;

			spinner = FindViewById<Spinner> (Resource.Id.attackMovesSpinner);

			//spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (experienceSpinner_ItemSelected);
			var adapter2 = ArrayAdapter.CreateFromResource (
				this, Resource.Array.attackMoves, Android.Resource.Layout.SimpleSpinnerItem);

			adapter2.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinner.Adapter = adapter2;

			spinner = FindViewById<Spinner> (Resource.Id.defenderSpinner);

			//spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (experienceSpinner_ItemSelected);
			var adapter3 = ArrayAdapter.CreateFromResource (
				this, Resource.Array.defender_experienceLevel, Android.Resource.Layout.SimpleSpinnerItem);

			adapter3.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spinner.Adapter = adapter3;

			Button button = FindViewById<Button> (Resource.Id.closeCombatCalc);
			button.Click += delegate {
				Calculate();
			};
		}

		void Calculate()
		{
			Spinner spinner = FindViewById<Spinner> (Resource.Id.attackerSpinner);

			int attackerLevel = spinner.SelectedItemPosition;

			spinner = FindViewById<Spinner> (Resource.Id.defenderSpinner);

			int defenderLevel = spinner.SelectedItemPosition;

			EditText skillInput = FindViewById<EditText> (Resource.Id.numberOfAttackers);

			if (skillInput.Text == "" || attackerLevel == 0 || defenderLevel == 0) {
				// Toast an error here
			} else {
				ExperienceLevels attackerExperience = (ExperienceLevels)(attackerLevel - 1);

				ExperienceLevels defenderExperience = (ExperienceLevels)(defenderLevel - 1);

				// Calculate the percentage for the values
				int baseCombat = Utilities.GetCombatPercentage(attackerExperience, defenderExperience);

				spinner = FindViewById<Spinner> (Resource.Id.attackMovesSpinner);

				CloseCombatAttacks attack = (CloseCombatAttacks)spinner.SelectedItemPosition;

				switch (attack) {
				case CloseCombatAttacks.AllOutAttack:
					baseCombat -= 10;
					break;
				case CloseCombatAttacks.ChargeForwardandAttack:
					baseCombat += 20;
					break;
				case CloseCombatAttacks.Trip:
				case CloseCombatAttacks.Breakaway:
				case CloseCombatAttacks.CircleAttack:
				case CloseCombatAttacks.FeintandAttack:
					baseCombat -= 20;
					break;
				case CloseCombatAttacks.StandardAttack:
				default:
					break;
				}

				int numberOfAttackers = Convert.ToInt32 (skillInput.Text);

				if (numberOfAttackers > 1) {
					baseCombat += (numberOfAttackers * 20);
				}

				TextView text = FindViewById<TextView> (Resource.Id.attackResults);
				text.Text = baseCombat.ToString () + GetString (Resource.String.end_shooting);
			}

		}
	}
}

