
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	[Activity (Label = "Spotting Calculator", Icon = "@drawable/ic_action_soldier")]
	public class SpottingCalculatorV2Activity : Activity
	{
		//SpottingCalculator spottingCalculator;
		CalculatorLogicV2 calculatorLogic;

		Spinner enviromental, targetStance, targetShooting, targetCover, spotterHealth, spotterStance, elevationDifference;
		MultiSelectSpinner  targetMisc, spotterMisc, targetVehicle, spotterVehicle;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.SpottingCalculatorV2);

			calculatorLogic = new CalculatorLogicV2 (this, CalculatorType.Spotting);

			// Enviromental
			enviromental = FindViewById<Spinner> (Resource.Id.EnviromentalSpinner);

			enviromental.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			ArrayAdapter adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Enviromental, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			enviromental.Adapter = adapter;


			//Target Stance
			targetStance = FindViewById<Spinner> (Resource.Id.TargetStanceSpinner);

			targetStance.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Stances, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			targetStance.Adapter = adapter;

			//Target Shooting
			targetShooting = FindViewById<Spinner> (Resource.Id.TargetShootingSpinner);

			targetShooting.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.targetShooting, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			targetShooting.Adapter = adapter;

			//Target Cover
			targetCover = FindViewById<Spinner> (Resource.Id.TargetCoverSpinner);

			targetCover.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Cover, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			targetCover.Adapter = adapter;

			//Target Misc
			targetMisc = FindViewById<MultiSelectSpinner> (Resource.Id.TargetMiscSpinner);

			targetMisc.SetItems (Resources.GetStringArray(Resource.Array.targetMisc));

			//Spotter Health
			spotterHealth = FindViewById<Spinner> (Resource.Id.SpotterHealthSpinner);

			spotterHealth.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.healthStates, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spotterHealth.Adapter = adapter;

			//Spotter Stance
			spotterStance = FindViewById<Spinner> (Resource.Id.SpotterStanceSpinner);

			spotterStance.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Stances, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			spotterStance.Adapter = adapter;

			//Spotter Misc
			spotterMisc = FindViewById<MultiSelectSpinner> (Resource.Id.SpotterMiscSpinner);

			spotterMisc.SetItems (Resources.GetStringArray(Resource.Array.spotterMisc));

			//Height Difference
			elevationDifference = FindViewById<Spinner> (Resource.Id.HeightDifferenceSpinner);

			elevationDifference.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			adapter = ArrayAdapter.CreateFromResource (
				this, Resource.Array.Elevation, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);

			elevationDifference.Adapter = adapter;
			elevationDifference.SetSelection (3);

			//targetVehicle
			targetVehicle = FindViewById<MultiSelectSpinner> (Resource.Id.TargetVehicleSpinner);

			targetVehicle.SetItems (Resources.GetStringArray(Resource.Array.targetVehicle));

			//spottingVehicle
			spotterVehicle = FindViewById<MultiSelectSpinner> (Resource.Id.SpottingVehicleSpinner);

			spotterVehicle.SetItems (Resources.GetStringArray(Resource.Array.spotterVehicle));

			// Calculate button
			Button pushButton = FindViewById<Button> (Resource.Id.spotCalculate);
			pushButton.Click += delegate {
				Calculate();
			};

		}

		public void Calculate ()
		{
			int additionalTargets;

			bool validTargets = int.TryParse((FindViewById<TextView> (Resource.Id.spotAdditionalEnemies).Text), out additionalTargets);

			if (!validTargets) {
				additionalTargets = 0;
			}

			//enviromental
			enum_Enviromental temp_enviro = (enum_Enviromental)enviromental.SelectedItemPosition;
			Modifiers enviroMod = Modifiers.None;

			switch (temp_enviro)
			{
				case enum_Enviromental.Clear:
					enviroMod = Modifiers.None;
					break;
				case enum_Enviromental.DuskDawn:
					enviroMod = Modifiers.TimeIsDuskorDawn;
					break;
				case enum_Enviromental.Night:
					enviroMod = Modifiers.TimeIsNight;
					break;
				default:
					break;
			}

			// target stance
			enum_Stances temp_targetStance = (enum_Stances)targetStance.SelectedItemPosition;
			Modifiers targetStanceMod = Modifiers.None;

			switch (temp_targetStance)
			{
				case enum_Stances.Standing:
					targetStanceMod = Modifiers.None;
					break;
				case enum_Stances.Running:
					targetStanceMod = Modifiers.TargetRunning;
					break;
				case enum_Stances.Walking:
					targetStanceMod = Modifiers.TargetWalking;
					break;
				case enum_Stances.Kneeling:
					targetStanceMod = Modifiers.TargetKneeling;
					break;
				case enum_Stances.Crawling:
					targetStanceMod = Modifiers.TargetCrawling;
					break;
				case enum_Stances.Prone:
					targetStanceMod = Modifiers.TargetProne;
					break;
				default:
					break;
			}

			// targetShooting
			enum_SpottingTargetShooting temp_targetShooting = (enum_SpottingTargetShooting) targetShooting.SelectedItemPosition;
			Modifiers targetShootingMod = Modifiers.None;

			switch (temp_targetShooting)
			{
				case enum_SpottingTargetShooting.NotShooting:
					targetShootingMod = Modifiers.None;
					break;
				case enum_SpottingTargetShooting.ShootingBoltAction:
					targetShootingMod = Modifiers.TargetFiringBoltActionRifle;
					break;
				case enum_SpottingTargetShooting.ShootingAutomatic:
					targetShootingMod = Modifiers.TargetFiringAutomaticWeapon;
					break;
				case enum_SpottingTargetShooting.ShootingHeavy:
					targetShootingMod = Modifiers.TargetFiringHeavyWeapon;
					break;
				default:
					break;
			}

			// targetCover
			enum_Cover temp_targetCover = (enum_Cover) targetCover.SelectedItemPosition;
			Modifiers targetCoverMod = Modifiers.None;

			switch (temp_targetCover)
			{
				case enum_Cover.None:
					targetCoverMod = Modifiers.None;
					break;
				case enum_Cover.Soft:
					targetCoverMod = Modifiers.TargetInSoftCover;
					break;
				case enum_Cover.Medium:
					targetCoverMod = Modifiers.TargetInMediumCover;
					break;
				case enum_Cover.Hard:
					targetCoverMod = Modifiers.TargetInHardCover;
					break;
				case enum_Cover.Prepared:
					targetCoverMod = Modifiers.TargetInPreparedDefences;
					break;
				default:
					break;
			}

			// targetMisc Multi
			List<int> targetMiscIndexs = targetMisc.GetSelectedIndicies();
			List<Modifiers> targetMiscMod = new List<Modifiers>();

			foreach(int i in targetMiscIndexs)
			{
				enum_SpottingTargetMisc misc = (enum_SpottingTargetMisc)i;

				switch (misc)
				{
					case enum_SpottingTargetMisc.UnderBarage:
						targetMiscMod.Add(Modifiers.TargetUnderBarage);
						break;
					case enum_SpottingTargetMisc.Hidden:
						targetMiscMod.Add(Modifiers.TargetHidden);
						break;
					case enum_SpottingTargetMisc.GhilleSuit:
						targetMiscMod.Add(Modifiers.TargetWearingGhilleSuit);
						break;
					case enum_SpottingTargetMisc.UnderFire:
						targetMiscMod.Add(Modifiers.TargetUnderFire);
						break;
					default:
						break;
				}
			}

			// spotterInjury
			InjuryLevel temp_spotterInjury = (InjuryLevel) spotterHealth.SelectedItemPosition;

			// spotterStance
			enum_Stances temp_spotterStance = (enum_Stances) spotterStance.SelectedItemPosition;
			Modifiers spotterStanceMod = Modifiers.None;

			switch (temp_spotterStance)
			{
				case enum_Stances.Standing:
				case enum_Stances.Kneeling:
				case enum_Stances.Crawling:
				case enum_Stances.Prone:
					spotterStanceMod = Modifiers.None;
					break;
				case enum_Stances.Running:
					spotterStanceMod = Modifiers.ActiveSoldierRunning;
					break;
				case enum_Stances.Walking:
					spotterStanceMod = Modifiers.ActiveSoldierWalking;
					break;
				default:
					break;
			}

			// spotterMisc Multi
			List<int> spotterMiscIndexs = spotterMisc.GetSelectedIndicies();
			List<Modifiers> spotterMiscMod = new List<Modifiers>();

			foreach (int i in spotterMiscIndexs)
			{
				enum_SpottingSpotterMisc spottingMisc = (enum_SpottingSpotterMisc) i;

				switch (spottingMisc)
				{
					case enum_SpottingSpotterMisc.SniperScope:
						spotterMiscMod.Add(Modifiers.ActiveSoldierUsingSniperScope);
						break;
					case enum_SpottingSpotterMisc.CloseToExplosion:
						spotterMiscMod.Add(Modifiers.ActiveSoldierWithin12inchofExplosion);
						break;
					case enum_SpottingSpotterMisc.ThermalOptics:
						spotterMiscMod.Add(Modifiers.ActiveSoldierUsingThermalOptics);
						break;
					case enum_SpottingSpotterMisc.NVGs:
						spotterMiscMod.Add(Modifiers.ActiveSoldierUsingNVGs);
						break;
					default:
						break;
				}

			}

			// elevationDifferenes
			enum_elevationDifferences temp_elevation = (enum_elevationDifferences) elevationDifference.SelectedItemPosition;

			// vehiclesTarget Multi
			List<int> targetMiscVIndex = targetVehicle.GetSelectedIndicies();
			List<Modifiers> vehiclesTargetMod = new List<Modifiers>();

			foreach (int i in targetMiscVIndex)
			{
				enum_SpottingTargetVMisc targetMiscV = (enum_SpottingTargetVMisc)i;
				{
					switch (targetMiscV)
					{
						case enum_SpottingTargetVMisc.VMoving:
							targetMiscMod.Add(Modifiers.TargetVehicleMoving);
							break;
						case enum_SpottingTargetVMisc.VFiringSecondary:
							vehiclesTargetMod.Add(Modifiers.TargetVehicleFiringSecondary);
							break;
						case enum_SpottingTargetVMisc.VFiringPrimary:
							vehiclesTargetMod.Add(Modifiers.TargetVehicleFiringPrimary);
							break;
						case enum_SpottingTargetVMisc.VSideOn:
							vehiclesTargetMod.Add(Modifiers.TargetVehicleSideOn);
							break;
						case enum_SpottingTargetVMisc.VHardCover:
							vehiclesTargetMod.Add(Modifiers.TargetVehicleHardCover);
							break;
						case enum_SpottingTargetVMisc.VSoftCover:
							vehiclesTargetMod.Add(Modifiers.TargetVehicleSoftCover);
							break;
						case enum_SpottingTargetVMisc.VHullDown:
							vehiclesTargetMod.Add(Modifiers.TargetVehicleHullDown);
							break;
						default:
							break;
					}
				}

			}

			// vehivlesSpotter Multi
			List<int> vehiclesSpotterIndex = spotterVehicle.GetSelectedIndicies();
			List<Modifiers> vehiclesSpotterMod = new List<Modifiers>();

			foreach (int i in vehiclesSpotterIndex)
			{
				enum_SpottingSpottingVMisc spotterMiscV = (enum_SpottingSpottingVMisc)i;

				switch (spotterMiscV)
				{
					case enum_SpottingSpottingVMisc.VButtonedUp:
						vehiclesSpotterMod.Add(Modifiers.ActiveVehicleButtonedUp);
						break;
					case enum_SpottingSpottingVMisc.VMoving:
						vehiclesSpotterMod.Add(Modifiers.ActiveVehicleMoving);
						break;
					default:
						break;
				}

			}


			int calculatedValue = calculatorLogic.CalculateSpotting (100, additionalTargets, enviroMod, targetStanceMod, targetShootingMod, targetCoverMod, targetMiscMod, temp_spotterInjury,
									  spotterStanceMod, spotterMiscMod, temp_elevation, vehiclesTargetMod, vehiclesSpotterMod);
			TextView result = FindViewById<TextView> (Resource.Id.spottingValuesField);

			result.Text = calculatedValue.ToString() + Resources.GetString (Resource.String.end_spotting);

		}

		public void Spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
		{

		}
	}
		
}

