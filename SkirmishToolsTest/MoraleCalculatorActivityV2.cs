
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	[Activity (Label = "Morale Calculator", Icon = "@drawable/ic_action_soldier")]
	public class MoraleCalculatorV2Activity : Activity
	{
		//SpottingCalculator spottingCalculator;
		CalculatorLogicV2 calculatorLogic;

		Spinner activeSoldierCover;
		MultiSelectSpinner activeSoldierDetails;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.MoraleCalculatorV2);

			calculatorLogic = new CalculatorLogicV2 (this, CalculatorType.Morale);

			// activeSoldierCOver
			activeSoldierCover = FindViewById<Spinner> (Resource.Id.ActiveSoldierCoverSpinner);

			activeSoldierCover.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (Spinner_ItemSelected);
			ArrayAdapter adapter = ArrayAdapter.CreateFromResource (
				                       this, Resource.Array.Cover, Android.Resource.Layout.SimpleSpinnerItem);

			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			activeSoldierCover.Adapter = adapter;

			//activeSoldierDetails
			activeSoldierDetails = FindViewById<MultiSelectSpinner> (Resource.Id.ActiveSoldierDetailsSpinner);

			activeSoldierDetails.SetItems (Resources.GetStringArray (Resource.Array.moraleMisc));

			// Calculate button
			Button pushButton = FindViewById<Button> (Resource.Id.spotCalculate);
			pushButton.Click += delegate {
				Calculate ();
			};

		}

		public void Calculate ()
		{
			int moraleValue, conciousSquadMemembersInt, enemyFightersInt, MoraleMarkersInt;

			// morale value
			bool moraleValueBool = int.TryParse ((FindViewById<TextView> (Resource.Id.moraleValue).Text), out moraleValue);

			if (!moraleValueBool) {
				moraleValue = 35;
			}

			if (moraleValue > 110) {
				moraleValue = 110;
			} else if (moraleValue < 35) {
				moraleValue = 35;
			}

			// conciousSquad
			bool conciousSquadBool = int.TryParse ((FindViewById<TextView> (Resource.Id.conciousFriendliesValue).Text), out conciousSquadMemembersInt);

			if (!conciousSquadBool) {
				conciousSquadMemembersInt = 0;
			}

			if (conciousSquadMemembersInt > 3) {
				conciousSquadMemembersInt = 3;
			} else if (conciousSquadMemembersInt < 0) {
				conciousSquadMemembersInt = 0;
			}

			// enemy fights
			bool enemyfightersBool = int.TryParse ((FindViewById<TextView> (Resource.Id.enemyFightersValue).Text), out enemyFightersInt);

			if (!enemyfightersBool) {
				enemyFightersInt = 0;
			}

			if (enemyFightersInt > 4) {
				enemyFightersInt = 4;
			} else if (enemyFightersInt < 0) {
				enemyFightersInt = 0;
			}

			// enemy fights
			bool moraleMarkersBool = int.TryParse ((FindViewById<TextView> (Resource.Id.numberOfMoraleMarkersValue).Text), out MoraleMarkersInt);

			if (!moraleMarkersBool) {
				MoraleMarkersInt = 1;
			}

			if (MoraleMarkersInt < 1) {
				MoraleMarkersInt = 1;
			}

			// targetCover
			enum_Cover temp_targetCover = (enum_Cover)activeSoldierCover.SelectedItemPosition;
			Modifiers targetCoverMod = Modifiers.None;

			switch (temp_targetCover) {
			case enum_Cover.None:
				targetCoverMod = Modifiers.None;
				break;
			case enum_Cover.Soft:
				targetCoverMod = Modifiers.TargetInSoftCover;
				break;
			case enum_Cover.Medium:
				targetCoverMod = Modifiers.TargetInMediumCover;
				break;
			case enum_Cover.Hard:
			case enum_Cover.Prepared:
				targetCoverMod = Modifiers.TargetInHardCover;
				break;
			default:
				break;
			}

			// targetMisc Multi
			List<int> targetMiscIndexs = activeSoldierDetails.GetSelectedIndicies ();
			List<Modifiers> targetMiscMod = new List<Modifiers> ();

			foreach (int i in targetMiscIndexs) {
				enum_MoraleMisc misc = (enum_MoraleMisc)i;

				switch (misc) {
				case enum_MoraleMisc.InflictedWounds:
					targetMiscMod.Add (Modifiers.ActiveSoldierInflictedWounds);
					break;
				case enum_MoraleMisc.ShowOfForce:
					targetMiscMod.Add (Modifiers.ShowOfForce);
					break;
				case enum_MoraleMisc.UnderFireMG:
					targetMiscMod.Add (Modifiers.MoraleUnderFireMG);
					break;
				case enum_MoraleMisc.UnderFireExplo:
					targetMiscMod.Add (Modifiers.MoraleUnderFireExplo);
					break;
				case enum_MoraleMisc.UnderFireHeavy:
					targetMiscMod.Add (Modifiers.MoraleUnderFireHeavy);
					break;
				case enum_MoraleMisc.InVehicle:
					targetMiscMod.Add (Modifiers.ActiveSoldierInVehicle);
					break;
				case enum_MoraleMisc.InjuredFriendlies:
					targetMiscMod.Add (Modifiers.MoraleInjuredNearby);
					break;
				}
			}


			int calculatedValue = calculatorLogic.CalculateMorale (moraleValue, conciousSquadMemembersInt, enemyFightersInt, MoraleMarkersInt, targetCoverMod, targetMiscMod);
			TextView result = FindViewById<TextView> (Resource.Id.spottingValuesField);

			result.Text = calculatedValue.ToString () + Resources.GetString (Resource.String.end_morale);

		}

		public void Spinner_ItemSelected (object sender, AdapterView.ItemSelectedEventArgs e)
		{

		}
	}
		
}

