﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	public enum CalculatorType
	{
		Spotting,
		Shooting,
		Morale,
		Grenade
	}

	public class CalculatorAdaptor : BaseAdapter<string>
	{
		public List<string> items;
		Activity context;

		CalculatorType calcType;

		string chapterHeader1 = "butts";
		string chapterHeader2 = "butts";

		public CalculatorAdaptor(Activity context, CalculatorType listType) : base()
		{
			this.context = context;
			calcType = listType;

			switch (listType) {
			case CalculatorType.Morale:
				SetupMorale ();
				break;
			case CalculatorType.Shooting:
				SetupShooting ();
				break;
			case CalculatorType.Spotting:
				SetupSpotting ();
				break;
			case CalculatorType.Grenade:
				SetupGrenade ();
				break;
			}
		}

		void SetupSpotting()
		{
			items = new List<string>(context.Resources.GetStringArray (Resource.Array.spotting_string_list));

			chapterHeader1 = context.Resources.GetString (Resource.String._SPOTTER);
			chapterHeader2 = context.Resources.GetString (Resource.String._TARGET);
		}

        void SetupShooting()
		{
			items = new List<string>(context.Resources.GetStringArray (Resource.Array.shooting_string_list));

			chapterHeader1 = context.Resources.GetString (Resource.String._SHOOTER);
			chapterHeader2 = context.Resources.GetString (Resource.String._TARGET);
		}

		void SetupGrenade()
		{
			items = new List<string>(context.Resources.GetStringArray (Resource.Array.grenade_string_list));

			chapterHeader1 = context.Resources.GetString (Resource.String._SHOOTER);
			chapterHeader2 = context.Resources.GetString (Resource.String._TARGET);
		}

        void SetupMorale()
		{
			items = new List<string>(context.Resources.GetStringArray (Resource.Array.morale_string_list));

			chapterHeader1 = context.Resources.GetString (Resource.String._SHOOTER);
			chapterHeader2 = context.Resources.GetString (Resource.String._TARGET);
		}

		#region adapter stuff
		public override long GetItemId(int position)
		{
			return position;
		}

		public override string this[int position]
		{   
			get { return items[position]; } 
		}

		public override int Count
		{
			get { return items.Count; } 
		}
		#endregion

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView; // re-use an existing view, if one is supplied
			if (view == null) { // otherwise create a new one
				if (items [position] == chapterHeader1 || items [position] == chapterHeader2) {
					view = context.LayoutInflater.Inflate (Android.Resource.Layout.SimpleListItem1 , null);
				} else {
					view = context.LayoutInflater.Inflate (Android.Resource.Layout.SimpleListItemChecked, null);
				}
			}
                
            // set view properties to reflect data for the given row
            view.FindViewById<TextView>(Android.Resource.Id.Text1).Text = items[position];
            // return the view, populated with data, for display
            return view;
        }
	}
}