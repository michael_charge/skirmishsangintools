﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace SkirmishTools
{
	[Activity (Label = "Morale Calculator", Icon = "@drawable/ic_action_soldier")]
	public class MoraleCalculatorActivity : Activity
	{
		//MoraleCalculator moraleCalculator;
		CalculatorLogic moraleCalculator;

		public List<int> checkedBoxes = new List<int> ();

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.MoraleCalculator);

			moraleCalculator = new CalculatorLogic (this, CalculatorType.Morale);

			// get all checkboxes and give them the OnCheckedChange

			ListView checkboxList = FindViewById<ListView> (Resource.Id.shootingList);


			CalculatorAdaptor adapter = new CalculatorAdaptor (this, CalculatorType.Morale);

			checkboxList.Adapter = adapter;
			checkboxList.ChoiceMode = ChoiceMode.Multiple;

			Button pushButton = FindViewById<Button> (Resource.Id.shootCalculate);
			pushButton.Click += delegate {
				Calculate ();
			};

		}

		public void Calculate ()
		{
			ListView checkboxList = FindViewById<ListView> (Resource.Id.shootingList);

			CalculatorAdaptor adapter = (CalculatorAdaptor)checkboxList.Adapter;

			EditText skillInput = FindViewById<EditText> (Resource.Id.shootBodyValue);

			if (skillInput.Text != "") {

				for (int i = 0; i < adapter.items.Count; i++) {
					if (checkboxList.IsItemChecked (i)) {
						checkedBoxes.Add (i);
					}
				}

				// Get button value
				int shootingSkill = Convert.ToInt32 (skillInput.Text);

				if (shootingSkill > 100) {
					shootingSkill = 100;
				} else if (shootingSkill <= 0) {
					shootingSkill = 22;
				}

				int calculatedValue = moraleCalculator.Calculate (checkedBoxes, shootingSkill, 0);

				TextView result = FindViewById<TextView> (Resource.Id.spottingValuesField);

				result.Text = calculatedValue.ToString () + Resources.GetString (Resource.String.end_morale);

				checkedBoxes.Clear ();
			} else {
				// Toast asking for value
				Toast.MakeText (this, Resources.GetString(Resource.String.enterMoraleValue), ToastLength.Long).Show();
			}


		}

		public void SetChecked (object sender, AdapterView.ItemClickEventArgs e)
		{
			ListView list = (ListView)sender;

			if (list.IsItemChecked (e.Position)) {
				list.SetItemChecked (e.Position, false);

			} else {
				list.SetItemChecked (e.Position, true);
			}
		}
	}
}