﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace SkirmishTools
{
	[Activity (Label = "SkirmishTools", MainLauncher = true, Icon = "@drawable/ic_soldier")]
	public class MainActivity : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);


			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Setup Buttons
			buttonSetup();
		}

		void buttonSetup()
		{
			Button spottingCalculatorButton = FindViewById<Button> (Resource.Id.Menu_SpottingCalc);

			spottingCalculatorButton.Click += delegate {
				GoToSpottingCalculator();
			};

			Button combatCalcButton = FindViewById<Button> (Resource.Id.Menu_CombatPopup);

			combatCalcButton.Click += delegate {
				PopupMenu menu = new PopupMenu (this, combatCalcButton);
				menu.Inflate (Resource.Menu.combatMenu);

				menu.MenuItemClick += (s1, arg1) => {
					switch(arg1.Item.ItemId)
					{
					case Resource.Id.popupShooting:
						GoToShootingCalculator();
						break;
					case Resource.Id.popupGrenade:
						GoToGrenadeCalculator();
						break;
					case Resource.Id.popupCloseCombat:
						GoToCombatCalculator();
						break;
					}
				};

				menu.Show ();
			};

//			Button shootingCalculatorButton = FindViewById<Button> (Resource.Id.Menu_ShootingCalc);
//
//			shootingCalculatorButton.Click += delegate {
//				GoToShootingCalculator();
//			};
//
//			Button grenadeCalculatorButton = FindViewById<Button> (Resource.Id.Menu_GrenadeCalculator);
//
//			grenadeCalculatorButton.Click += delegate {
//				GoToGrenadeCalculator();
//			};

			Button moraleCalculatorButton = FindViewById<Button> (Resource.Id.Menu_MoraleCalc);

			moraleCalculatorButton.Click += delegate {
				GoToMoraleCalculator();
			};

			Button soldierEditorButton = FindViewById<Button> (Resource.Id.Menu_SoldierEdit);

			soldierEditorButton.Click += delegate {
				GoToSoldierEditor();
			};

//			Button armyListButton = FindViewById<Button> (Resource.Id.Menu_ArmyLists);
//
//			armyListButton.Click += delegate {
//				GoToArmyLists();
//			};

			// Links
			Button store = FindViewById<Button> (Resource.Id.Menu_Store);

			store.Click += delegate {
				GoToStore();
			};
			Button forum = FindViewById<Button> (Resource.Id.Menu_Forum);

			forum.Click += delegate {
				GoToForum();
			};
			Button blog = FindViewById<Button> (Resource.Id.Menu_Blog);

			blog.Click += delegate {
				GoToBlog();
			};
			Button developer = FindViewById<Button> (Resource.Id.Menu_Developer);

			developer.Click += delegate {
				GoToDeveloper();
			};

		}

		// Go To Buttons
		public void GoToSpottingCalculator()
		{
			var intent = new Intent(this, typeof(SpottingCalculatorV2Activity));
			StartActivity(intent);
		}

		public void GoToShootingCalculator()
		{
			var intent = new Intent(this, typeof(ShootingCalculatorV2Activity));
			StartActivity(intent);
		}

		public void GoToGrenadeCalculator()
		{
			var intent = new Intent(this, typeof(GrenadeCalculatorV2Activity));
			StartActivity(intent);
		}

		public void GoToCombatCalculator()
		{
			var intent = new Intent(this, typeof(CloseCombatCalculatorActivity));
			StartActivity(intent);
		}

		public void GoToMoraleCalculator()
		{
			var intent = new Intent(this, typeof(MoraleCalculatorV2Activity));
			StartActivity(intent);
		}

		public void GoToSoldierEditor()
		{
			var intent = new Intent(this, typeof(SoldierEditorActivity));
			StartActivity(intent);
		}

		public void GoToArmyLists()
		{
			var intent = new Intent(this, typeof(ArmyListEditorActivity));
			StartActivity(intent);
		}

		public void GoToStore()
		{
            var uri = Android.Net.Uri.Parse("http://www.radiodishdash.co.uk/");
			var intent = new Intent (Intent.ActionView, uri);
			StartActivity (intent);
		}

		public void GoToForum()
		{
            var uri = Android.Net.Uri.Parse("http://skirmishsangin.freeforums.net/");
			var intent = new Intent (Intent.ActionView, uri);
			StartActivity (intent);
		}

		public void GoToBlog()
		{
            var uri = Android.Net.Uri.Parse("http://skirmishsangin.blogspot.co.nz/");
			var intent = new Intent (Intent.ActionView, uri);
			StartActivity (intent);
		}

		public void GoToDeveloper()
		{
            var uri = Android.Net.Uri.Parse("https://twitter.com/ChargeDog");
			var intent = new Intent (Intent.ActionView, uri);
			StartActivity (intent);
		}

	}
}


